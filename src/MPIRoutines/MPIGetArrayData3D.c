#include <stdio.h>
#include <stdlib.h>
#include <arrayfunctions.h>
#ifndef serial
#include <mpi.h>
#endif
#include <mpivars.h>

int MPIGetArrayData3D(double *xbuf,double *x,int *source,int *dest,int *limits,int *dimensions,void *m)
{
  MPIVariables *mpi  = (MPIVariables*) m;
  int           ierr = 0;

  int source_ip = source[0];
  int source_jp = source[1];
  int source_kp = source[2];
  int dest_ip   = dest[0];
  int dest_jp   = dest[1];
  int dest_kp   = dest[2];

  int source_rank,dest_rank;
  ierr = rank1D(&source_rank,mpi->iproc,mpi->jproc,mpi->kproc,source_ip,source_jp,source_kp);
  if (ierr) return(ierr);
  ierr = rank1D(&dest_rank  ,mpi->iproc,mpi->jproc,mpi->kproc,dest_ip  ,dest_jp  ,dest_kp  );
  if (ierr) return(ierr);

  int is,ie,js,je,ks,ke;
  is = limits[0];
  ie = limits[1];
  js = limits[2];
  je = limits[3];
  ks = limits[4];
  ke = limits[5];

  int imax,jmax,kmax,ghosts;
  imax   = dimensions[0];
  jmax   = dimensions[1];
  kmax   = dimensions[2];
  ghosts = dimensions[3];

  if (source_rank == dest_rank) {
    /* source and dest are the same process */
    int i,j,k;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int p = Index1D(i   ,j   ,k   ,imax ,jmax ,kmax ,ghosts);
          int q = Index1D(i-is,j-js,k-ks,ie-is,je-js,ke-ks,0     );
          xbuf[q] = x[p];
        }
      }
    }
    return(0);

  } else {
#ifdef serial
    fprintf(stderr,"Error in MPIGetArrayData3D(): This is a serial run. Source and ");
    fprintf(stderr,"destination ranks must be equal. Instead they are %d and %d.\n",
                    source_rank,dest_rank);
    return(1);
#else
    if (mpi->rank == source_rank) {
      double *buf = (double*) calloc ((ie-is)*(je-js)*(ke-ks),sizeof(double));
      int i,j,k;
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int p = Index1D(i   ,j   ,k   ,imax ,jmax ,kmax ,ghosts);
            int q = Index1D(i-is,j-js,k-ks,ie-is,je-js,ke-ks,0     );
            buf[q] = x[p];
          }
        }
      }
      MPI_Send(buf,(ie-is)*(je-js)*(ke-ks),MPI_DOUBLE,dest_rank,2211,MPI_COMM_WORLD);
      free(buf);
    } else if (mpi->rank == dest_rank) {
      MPI_Status status;
      MPI_Recv(xbuf,(ie-is)*(je-js)*(ke-ks),MPI_DOUBLE,source_rank,2211,MPI_COMM_WORLD,&status);
    } else {
      fprintf(stderr,"Error in MPIGetArrayData3D(): Process %d shouldn't have entered this function.\n",mpi->rank);
      return(1);
    }

    return(0);
#endif
  }
}

