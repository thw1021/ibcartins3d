#include <arrayfunctions.h>

int ExtrapolateArray(double *x,double *y,int size,int g)
{
  int i;
  for (i = g; i < size+g; i++)        y[i] = x[i-g];
  for (i = size+g; i < size+2*g; i++) y[i] = y[i-1] + (y[i-1]-y[i-2]);
  for (i = g-1; i > -1; i--)          y[i] = y[i+1] + (y[i+1]-y[i+2]);
  return(0);
}

