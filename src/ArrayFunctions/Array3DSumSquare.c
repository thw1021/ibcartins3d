#include <math.h>
#include <arrayfunctions.h>

double Array3DSumSquare(double *x, int *dim)
{
  double norm = 0.0;

  int is,ie,js,je,ks,ke,imax,jmax,kmax,offset;
  is = dim[0]; ie = dim[1];
  js = dim[2]; je = dim[3];
  ks = dim[4]; ke = dim[5];
  imax    = dim[6];
  jmax    = dim[7];
  kmax    = dim[8];
  offset  = dim[9];

  int i,j,k;
  for (i = is; i < ie; i++) {
    for (j = js; j < je; j++) {
      for (k = ks; k < ke; k++) {
        int p = Index1D(i,j,k,imax,jmax,kmax,offset);
        norm += (x[p]*x[p]);
      }
    }
  }
  return(norm);
}
