#include <string.h>
#include <arrayfunctions.h>
#include <boundaryconditions.h>
#include <ibcartins3d.h>

int BCPressureSolid_imin(int *dim,double *phi,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* phi */
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k,imax,jmax,kmax,ghosts);
          int pi1 = Index1D(0,j,k,imax,jmax,kmax,ghosts);
          int pi2 = Index1D(1,j,k,imax,jmax,kmax,ghosts);
          phi[pb] = (4*phi[pi1] - phi[pi2]) / 3.0;
        }
      }
    }
  }

  return(0);
}

int BCPressureSolid_imax(int *dim,double *phi,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* phi */
    is = imax;
    ie = imax+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i     ,j,k,imax,jmax,kmax,ghosts);
          int pi1 = Index1D(imax-1,j,k,imax,jmax,kmax,ghosts);
          int pi2 = Index1D(imax-2,j,k,imax,jmax,kmax,ghosts);
          phi[pb] = (4*phi[pi1] - phi[pi2]) / 3.0;
        }
      }
    }
  }

  return(0);
}

int BCPressureSolid_jmin(int *dim,double *phi,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* phi */
    is = boundary->is;
    ie = boundary->ie;
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k,imax,jmax,kmax,ghosts);
          int pi1 = Index1D(i,0,k,imax,jmax,kmax,ghosts);
          int pi2 = Index1D(i,1,k,imax,jmax,kmax,ghosts);
          phi[pb] = (4*phi[pi1] - phi[pi2]) / 3.0;
        }
      }
    }
  }

  return(0);
}

int BCPressureSolid_jmax(int *dim,double *phi,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* phi */
    is = boundary->is;
    ie = boundary->ie;
    js = jmax;
    je = jmax+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j     ,k,imax,jmax,kmax,ghosts);
          int pi1 = Index1D(i,jmax-1,k,imax,jmax,kmax,ghosts);
          int pi2 = Index1D(i,jmax-2,k,imax,jmax,kmax,ghosts);
          phi[pb] = (4*phi[pi1] - phi[pi2]) / 3.0;
        }
      }
    }
  }

  return(0);
}

int BCPressureSolid_kmin(int *dim,double *phi,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* phi */
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k,imax,jmax,kmax,ghosts);
          int pi1 = Index1D(i,j,0,imax,jmax,kmax,ghosts);
          int pi2 = Index1D(i,j,1,imax,jmax,kmax,ghosts);
          phi[pb] = (4*phi[pi1] - phi[pi2]) / 3.0;
        }
      }
    }
  }

  return(0);
}

int BCPressureSolid_kmax(int *dim,double *phi,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* phi */
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k     ,imax,jmax,kmax,ghosts);
          int pi1 = Index1D(i,j,kmax-1,imax,jmax,kmax,ghosts);
          int pi2 = Index1D(i,j,kmax-2,imax,jmax,kmax,ghosts);
          phi[pb] = (4*phi[pi1] - phi[pi2]) / 3.0;
        }
      }
    }
  }

  return(0);
}

