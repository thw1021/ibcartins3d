#include <stdio.h>
#include <interpolation.h>

/* 
  Second order central interpolation (uniform grid)
*/

#undef  _MINIMUM_GHOSTS_
#define _MINIMUM_GHOSTS_ 1

int SecondOrderCentral(double *fI,double *fC,int N,int ghosts,int rank,int nproc,void *p)
{
  if ((!fI) || (!fC)) {
    fprintf(stderr, "Error in SecondOrderCentral(): input arrays not allocated.\n");
    return(1);
  }
  if (ghosts < _MINIMUM_GHOSTS_) {
    fprintf(stderr, "Error in SecondOrderCentral(): insufficient number of ghosts.\n");
    return(1);
  }

  int i;
  for (i = 0; i < N+1; i++) fI[i] = 0.5 * (fC[ghosts+i-1] + fC[ghosts+i]);
  
  return(0);
}
