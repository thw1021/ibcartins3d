#include <stdlib.h>
#include <interpolation.h>

static int WENOInitialize(void*);

int InterpInitialize(void *p,int scheme)
{
  InterpolationParameters *interp = (InterpolationParameters*) p;

  int ierr = 0;
  if (scheme == 5) {
    /* fifth order WENO scheme */
    interp->scheme = (WENOParameters*) calloc (1,sizeof(WENOParameters));
    ierr = WENOInitialize(interp->scheme); if (ierr) return(ierr);
  } else {
    interp->scheme = NULL;
  }
  return(0);
}

int WENOInitialize(void *p)
{
  WENOParameters *weno = (WENOParameters*) p;

  /* hard coding these parameters for now */
  /* modify to read from an input file later */
  weno->mapped      = 0;
  weno->borges      = 0;
  weno->yc          = 0;
  weno->no_limiting = 0;
  weno->eps         = 1e-6;
  weno->p           = 2.0;

  return(0);
}
