#include <string.h>
#include <stdlib.h>
#include <linearsolver.h>

int LSInitialize(void *ls,char *type,int maxiter,void *context, int *dim,double atol, double rtol,
                 int delta,int verbose,int (*BCFunction)(int*,double*,void*,void*,int,int))
{
  LinearSolver *LS    = (LinearSolver*) ls;
  int           ierr  = 0;

  strcpy(LS->type,type);
  LS->maxiter = maxiter;
  LS->context = context;
  LS->atol    = atol;
  LS->rtol    = rtol;
  LS->delta   = delta;
  LS->verbose = verbose;

  /* set boundary condition function */
  LS->ApplyBC = BCFunction; 

  /* hard-coded SIP as the linear solver */
  LS->Solver = SIPSolve;
  LS->scheme = (SIP*) calloc (1,sizeof(SIP));
  ierr = SIPInitialize(LS->scheme,dim[0],dim[1],dim[2]);
  if (ierr) return(ierr);

  return(0);
}

