#include <arrayfunctions.h>
#include <mpivars.h>
#include <linearsolver.h>

int SIPComputeCoefficients (void *s,void *m,int *dim,
                            double *x,double *y,double *z,
                            double diag,double coeff)
{
  SIP           *sip = (SIP*) s;
  MPIVariables  *mpi = (MPIVariables*) m;

  int imax = dim[0];
  int jmax = dim[1];
  int kmax = dim[2];

  int is   = mpi->is; int ip = mpi->ip; int iproc = mpi->iproc;
  int js   = mpi->js; int jp = mpi->jp; int jproc = mpi->jproc;
  int ks   = mpi->ks; int kp = mpi->kp; int kproc = mpi->kproc;

  int i,j,k;
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int pC = Index1D(i  ,j  ,k  ,imax,jmax,kmax,0);
        int pW = Index1D(i-1,j  ,k  ,imax,jmax,kmax,0);
        int pS = Index1D(i  ,j-1,k  ,imax,jmax,kmax,0);
        int pB = Index1D(i  ,j  ,k-1,imax,jmax,kmax,0);

        if ((k == 0) && (kp == 0)) {
          sip->Ab[pC] = coeff / (2.0*(z[ks+k+1]-z[ks+k]) * (z[ks+k+1]-z[ks+k]));
				  sip->At[pC] = coeff / (2.0*(z[ks+k+1]-z[ks+k]) * (z[ks+k+1]-z[ks+k]));
        } else if ((k == kmax-1) && (kp == kproc-1)) {
          sip->Ab[pC] = coeff / (2.0*(z[ks+k]-z[ks+k-1]) * (z[ks+k]-z[ks+k-1]));
				  sip->At[pC] = coeff / (2.0*(z[ks+k]-z[ks+k-1]) * (z[ks+k]-z[ks+k-1]));
        } else {
          sip->Ab[pC] = coeff / ((z[ks+k+1]-z[ks+k-1]) * (z[ks+k]-z[ks+k-1]));
				  sip->At[pC] = coeff / ((z[ks+k+1]-z[ks+k-1]) * (z[ks+k+1]-z[ks+k]));
        }
        if ((j == 0) && (jp == 0)) {
          sip->As[pC] = coeff / (2.0*(y[js+j+1]-y[js+j]) * (y[js+j+1]-y[js+j]));
          sip->An[pC] = coeff / (2.0*(y[js+j+1]-y[js+j]) * (y[js+j+1]-y[js+j]));
        } else if ((j == jmax-1) && (jp == jproc-1)) {
          sip->As[pC] = coeff / (2.0*(y[js+j]-y[js+j-1]) * (y[js+j]-y[js+j-1]));
          sip->An[pC] = coeff / (2.0*(y[js+j]-y[js+j-1]) * (y[js+j]-y[js+j-1]));
        } else {
          sip->As[pC] = coeff / ((y[js+j+1]-y[js+j-1]) * (y[js+j]-y[js+j-1]));
          sip->An[pC] = coeff / ((y[js+j+1]-y[js+j-1]) * (y[js+j+1]-y[js+j]));
        }
        if ((i == 0) && (ip == 0)) {
          sip->Aw[pC] = coeff / (2.0*(x[is+i+1]-x[is+i]) * (x[is+i+1]-x[is+i]));
          sip->Ae[pC] = coeff / (2.0*(x[is+i+1]-x[is+i]) * (x[is+i+1]-x[is+i]));
        } else if ((i == imax-1) && (ip == iproc-1)) {
          sip->Aw[pC] = coeff / (2.0*(x[is+i]-x[is+i-1]) * (x[is+i]-x[is+i-1]));
          sip->Ae[pC] = coeff / (2.0*(x[is+i]-x[is+i-1]) * (x[is+i]-x[is+i-1]));
        } else {
          sip->Aw[pC] = coeff / ((x[is+i+1]-x[is+i-1]) * (x[is+i]-x[is+i-1]));
          sip->Ae[pC] = coeff / ((x[is+i+1]-x[is+i-1]) * (x[is+i+1]-x[is+i]));
        }
				sip->Ac[pC] = diag - (sip->Ab[pC]+sip->As[pC]+sip->Aw[pC]+sip->Ae[pC]+sip->An[pC]+sip->At[pC]);

				sip->Ls[pC] = sip->As[pC] / (1 + (j == 0 ? 0 : sip->alpha*sip->Ue[pS] + sip->alpha*sip->Ut[pS]));
				sip->Lw[pC] = sip->Aw[pC] / (1 + (i == 0 ? 0 : sip->alpha*sip->Un[pW] + sip->alpha*sip->Ut[pW]));
				sip->Lb[pC] = sip->Ab[pC] / (1 + (k == 0 ? 0 : sip->alpha*sip->Ue[pB] + sip->alpha*sip->Un[pB]));

        double trm1, trm2, trm3;
				trm1 = sip->alpha * (sip->Lb[pC]*(k==0?0:sip->Un[pB]) + sip->Lw[pC]*(i==0?0:sip->Un[pW]));
				trm2 = sip->alpha * (sip->Lb[pC]*(k==0?0:sip->Ue[pB]) + sip->Ls[pC]*(j==0?0:sip->Ue[pS]));
				trm3 = sip->alpha * (sip->Lw[pC]*(i==0?0:sip->Ut[pW]) + sip->Ls[pC]*(j==0?0:sip->Ut[pS]));

				sip->Lc[pC]  =  sip->Ac[pC] + trm1 + trm2 + trm3;
				sip->Lc[pC] -= (sip->Lb[pC] * (k==0?0:sip->Ut[pB]));
        sip->Lc[pC] -= (sip->Lw[pC] * (i==0?0:sip->Ue[pW]));
				sip->Lc[pC] -= (sip->Ls[pC] * (j==0?0:sip->Un[pS]));
        sip->Lc[pC]  = 1.0 / sip->Lc[pC];

				sip->Ut[pC] = (sip->At[pC] - trm3) * sip->Lc[pC];
				sip->Un[pC] = (sip->An[pC] - trm1) * sip->Lc[pC];
				sip->Ue[pC] = (sip->Ae[pC] - trm2) * sip->Lc[pC];
      }
    }
  }
  return(0);
}
