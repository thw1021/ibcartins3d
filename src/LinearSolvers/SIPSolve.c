#include <stdio.h>
#include <stdlib.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <linearsolver.h>

int SIPSolve(void *ls,void *m,double *q,double *rhs,int *dim,int *shift,double *blank)
{
  LinearSolver  *solver = (LinearSolver*) ls;
  SIP           *sip    = (SIP*) solver->scheme;
  MPIVariables  *mpi    = (MPIVariables*) m;
  int           ierr    = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  int xshift  = shift[0];
  int yshift  = shift[1];
  int zshift  = shift[2];

  /* allocate arrays */
  double *dq1, *dq2;
  dq1 = (double*) calloc (imax*jmax*kmax,sizeof(double));
  dq2 = (double*) calloc (imax*jmax*kmax,sizeof(double));

  int     iter,maxiter = solver->maxiter,total_npoints;
  double  total_norm;
  for (iter = 0; iter < maxiter; iter++) {
    int i,j,k;

    /* apply boundary conditions on q if available */
    if (solver->ApplyBC) {
      ierr = solver->ApplyBC(dim,q,solver->context,mpi,solver->delta,0); 
      if (ierr) return(ierr);
    }

    /* exchange data over MPI boundaries */
    ierr = MPIExchangeBoundaries(imax,jmax,kmax,ghosts,xshift,yshift,zshift,mpi,q);
    if (ierr) return(ierr);

    /* evaluate residual */
    /* Note: SIP is meant for banded systems resulting from     */
    /* the discretized Laplacian operator only.                 */
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int p   = Index1D(i  ,j  ,k  ,imax,jmax,kmax,0     );
          int pE  = Index1D(i+1,j  ,k  ,imax,jmax,kmax,ghosts);
          int pW  = Index1D(i-1,j  ,k  ,imax,jmax,kmax,ghosts);
          int pN  = Index1D(i  ,j+1,k  ,imax,jmax,kmax,ghosts);
          int pS  = Index1D(i  ,j-1,k  ,imax,jmax,kmax,ghosts);
          int pT  = Index1D(i  ,j  ,k+1,imax,jmax,kmax,ghosts);
          int pB  = Index1D(i  ,j  ,k-1,imax,jmax,kmax,ghosts);
          int pC  = Index1D(i  ,j  ,k  ,imax,jmax,kmax,ghosts);
          dq1[p]  = rhs[p];
          dq1[p] -= sip->Ae[p] * q[pE];
          dq1[p] -= sip->Aw[p] * q[pW];
          dq1[p] -= sip->An[p] * q[pN];
          dq1[p] -= sip->As[p] * q[pS];
          dq1[p] -= sip->At[p] * q[pT];
          dq1[p] -= sip->Ab[p] * q[pB];
          dq1[p] -= sip->Ac[p] * q[pC];
        }
      }
    }

    /* calculate residual norm and check for convergence criteria */
    double norm    = 0,norm0;
    int    npoints = imax*jmax*kmax;
    for (i = 0; i < imax*jmax*kmax; i++) norm += (dq1[i] * dq1[i]);
    total_norm = 0;
    ierr = MPISum_double (&total_norm   , &norm   , 1);  if (ierr) return(ierr);
    ierr = MPISum_integer(&total_npoints, &npoints, 1);  if (ierr) return(ierr);
    total_norm /= total_npoints;
    if (!mpi->rank && solver->verbose) printf("    SIP: iter = %3d, norm = %E\n",iter,total_norm); 
    if (!iter)  {
      if (total_norm < solver->atol) break;
      else  norm0 = total_norm;
    } else {
      if ((total_norm/norm0 < solver->rtol) 
        || (total_norm < solver->atol)) break;
    }

    /* forward sweep */
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int pC, pW, pS, pB;
          pC = Index1D(i  ,j  ,k  ,imax,jmax,kmax,0);
          pW = Index1D(i-1,j  ,k  ,imax,jmax,kmax,0);
          pS = Index1D(i  ,j-1,k  ,imax,jmax,kmax,0);
          pB = Index1D(i  ,j  ,k-1,imax,jmax,kmax,0);

          double trm = dq1[pC];
          trm -= (i == 0 ? 0 : sip->Lw[pC] * dq2[pW]);
          trm -= (j == 0 ? 0 : sip->Ls[pC] * dq2[pS]);
          trm -= (k == 0 ? 0 : sip->Lb[pC] * dq2[pB]);
          dq2[pC] = trm * sip->Lc[pC];
        }
      }
    }

    /* backward sweep */
    for (i = imax-1; i > -1; i--) {
      for (j = jmax-1; j > -1; j--) {
        for (k = kmax-1; k > -1; k--) {
          int pC, pE, pN, pT;
          pC = Index1D(i  ,j  ,k  ,imax,jmax,kmax,0);
          pE = Index1D(i+1,j  ,k  ,imax,jmax,kmax,0);
          pN = Index1D(i  ,j+1,k  ,imax,jmax,kmax,0);
          pT = Index1D(i  ,j  ,k+1,imax,jmax,kmax,0);

          dq1[pC] = dq2[pC];
          dq1[pC] -= (i == imax-1 ? 0 : sip->Ue[pC] * dq1[pE]);
          dq1[pC] -= (j == jmax-1 ? 0 : sip->Un[pC] * dq1[pN]);
          dq1[pC] -= (k == kmax-1 ? 0 : sip->Ut[pC] * dq1[pT]);

          int pp = Index1D(i,j,k,imax,jmax,kmax,ghosts);
          q[pp] += dq1[pC];
        }
      }
    }
  }

  solver->exit_norm = total_norm;
  solver->exit_iter = iter + 1;

  /* clean up */
  free(dq1);
  free(dq2);
  return(0);
}
