#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <ibcartins3d.h>

/* Function declarations */
int InterpolateStagToColl   (double*,double*,double*,double*,double*,double*,int,int,int,int,int);
int IncrementFilename       (char*);
int ApplyBoundaryConditionsPressure  (int*,double*,void*,void*,int,int);
int ApplyBoundaryConditionsVelocity  (int*,double*,double*,double*,void*,void*,int,int);

int OutputSolution(void *s, void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  MPIVariables  *mpi    = (MPIVariables*)m;
  int           ierr    = 0, dimensions[7];

  /* if WriteOutput() is NULL, then return */
  if (!solver->WriteOutput) return(0);

  /* output solution will include 1 layer of boundary points */
  /* 5 layers of boundary points, if in debug mode           */
#ifdef debug
  int ghosts_op = solver->ghosts;
#else
  int ghosts_op = 0;
#endif

  int imax_local  = solver->imax_local;
  int jmax_local  = solver->jmax_local;
  int kmax_local  = solver->kmax_local;
  int imax_global = solver->imax_global;
  int jmax_global = solver->jmax_global;
  int kmax_global = solver->kmax_global;

  /* Apply boundary conditions before output */
  dimensions[0] = imax_local;  dimensions[4] = dimensions[0] + 1;
  dimensions[1] = jmax_local;  dimensions[5] = dimensions[1] + 1;
  dimensions[2] = kmax_local;  dimensions[6] = dimensions[2] + 1;
  dimensions[3] = solver->ghosts;
  ierr = ApplyBoundaryConditionsVelocity(&dimensions[0],solver->u,solver->v,solver->w,
                                         solver,mpi,0,1);
  if (ierr) return(ierr);
  ierr = ApplyBoundaryConditionsPressure(&dimensions[0],solver->phi,solver,mpi,0,1); 
  if(ierr) return(ierr);

  /* dimension of local arrays for output             */
  /* with a layer of ghost (boundary) points          */
  int dim =  (imax_local+2*ghosts_op)
            *(jmax_local+2*ghosts_op)
            *(kmax_local+2*ghosts_op);

  /* collocated velocity component arrays for output  */
  double *uc = solver->uc;
  double *vc = solver->vc;
  double *wc = solver->wc;
  /* local output arrays for phi and iblank           */
  double *phi    = solver->phic;
  double *iblank = solver->iblankc;
  /* initialize iblank as 1 before copying actual data */
  ierr = SetArrayValue(iblank,dim,1.0); if(ierr) return(ierr);

  /* Calculate collocated velocities from staggered velocities */
  double *u = solver->u;
  double *v = solver->v;
  double *w = solver->w;
  ierr = InterpolateStagToColl(u,v,w,uc,vc,wc,
                               imax_local,jmax_local,kmax_local,
                               solver->ghosts,ghosts_op);
  if (ierr) return(ierr);
  /* Copy phi and iblank into the output arrays */
  ierr = CopyArrayGhost1ToGhost2(imax_local,jmax_local,kmax_local,
                                 solver->ghosts,ghosts_op,
                                 solver->phi,phi);
  if (ierr) return(ierr);
  ierr = CopyArrayGhost1ToGhost2(imax_local,jmax_local,kmax_local,
                                 solver->ghosts,ghosts_op,
                                 solver->iblank,iblank);
  if (ierr) return(ierr);

  /* output arrays exchange MPI boundary data for consistency */
  ierr = MPIExchangeBoundaries(imax_local,jmax_local,kmax_local,ghosts_op,
                               0,0,0,mpi,uc);     if(ierr)  return(ierr);
  ierr = MPIExchangeBoundaries(imax_local,jmax_local,kmax_local,ghosts_op,
                               0,0,0,mpi,vc);     if(ierr)  return(ierr);
  ierr = MPIExchangeBoundaries(imax_local,jmax_local,kmax_local,ghosts_op,
                               0,0,0,mpi,wc);     if(ierr)  return(ierr);
  ierr = MPIExchangeBoundaries(imax_local,jmax_local,kmax_local,ghosts_op,
                               0,0,0,mpi,phi);    if(ierr)  return(ierr);
  ierr = MPIExchangeBoundaries(imax_local,jmax_local,kmax_local,ghosts_op,
                               0,0,0,mpi,iblank); if(ierr)  return(ierr);
  /* Apply boundary conditions to these local output arrays */
  dimensions[0] = imax_local;
  dimensions[1] = jmax_local;
  dimensions[2] = kmax_local;
  dimensions[3] = ghosts_op;
  dimensions[4] = dimensions[0]; /* uc is collocated */
  dimensions[5] = dimensions[1]; /* vc is collocated */
  dimensions[6] = dimensions[2]; /* wc is collocated */
  ierr = ApplyBoundaryConditionsVelocity(&dimensions[0],solver->u,solver->v,solver->w,
                                         solver,mpi,0,0);
  if (ierr) return(ierr);
  ierr = ApplyBoundaryConditionsPressure(&dimensions[0],solver->phi,solver,mpi,0,0); 
  /* Now these arrays are ready to be sent to root process */

  /* root process: allocate global output arrays */
  double *u_op, *v_op, *w_op, *p_op, *b_op;
  if (!mpi->rank) {
    int dim_global = (imax_global+2*ghosts_op)
                    *(jmax_global+2*ghosts_op)
                    *(kmax_global+2*ghosts_op);
    u_op = (double*) calloc (dim_global,sizeof(double));
    v_op = (double*) calloc (dim_global,sizeof(double));
    w_op = (double*) calloc (dim_global,sizeof(double));
    p_op = (double*) calloc (dim_global,sizeof(double));
    b_op = (double*) calloc (dim_global,sizeof(double));
    /* initialize n_op as 1 before copying actual data */
    ierr = SetArrayValue(b_op,dim_global,1.0); if(ierr) return(ierr);
    if (ierr) return(ierr);
  } else {
    /* null pointers on non-root processes */
    u_op = v_op = w_op = p_op = b_op = NULL;
  }

  int dim_global[3],dim_local[3];
  dim_global[0] = imax_global;
  dim_global[1] = jmax_global;
  dim_global[2] = kmax_global;
  dim_local[0]  = imax_local;
  dim_local[1]  = jmax_local;
  dim_local[2]  = kmax_local;

  /* Assemble the local output arrays into the global output arrays */
  ierr = MPIGatherArray3D(mpi,u_op,uc    ,&dim_global[0],&dim_local[0],ghosts_op);
  if (ierr) return(ierr);
  ierr = MPIGatherArray3D(mpi,v_op,vc    ,&dim_global[0],&dim_local[0],ghosts_op);
  if (ierr) return(ierr);
  ierr = MPIGatherArray3D(mpi,w_op,wc    ,&dim_global[0],&dim_local[0],ghosts_op);
  if (ierr) return(ierr);
  ierr = MPIGatherArray3D(mpi,p_op,phi   ,&dim_global[0],&dim_local[0],ghosts_op);
  if (ierr) return(ierr);
  ierr = MPIGatherArray3D(mpi,b_op,iblank,&dim_global[0],&dim_local[0],ghosts_op);
  if (ierr) return(ierr);

  if (!mpi->rank) {
    int imax = solver->imax_global;
    int jmax = solver->jmax_global;
    int kmax = solver->kmax_global;
    int dim_global = (imax+2*ghosts_op)*(jmax+2*ghosts_op)*(kmax+2*ghosts_op);

    /* allocate and set x,y,z arrays with the ghost (boundary) points */
    double *x_op, *y_op, *z_op;
    x_op = (double*) calloc (dim_global,sizeof(double));
    y_op = (double*) calloc (dim_global,sizeof(double));
    z_op = (double*) calloc (dim_global,sizeof(double));
    ierr = ExtrapolateArray(solver->xg,x_op,imax,ghosts_op); if(ierr) return(ierr);
    ierr = ExtrapolateArray(solver->yg,y_op,jmax,ghosts_op); if(ierr) return(ierr);
    ierr = ExtrapolateArray(solver->zg,z_op,kmax,ghosts_op); if(ierr) return(ierr);

    /* write output file to disk */
    char filename[_MAX_STRING_SIZE_] = "";
    strcat(filename,solver->op_prefix);
    strcat(filename,solver->op_filename);
    int dimensions[3];
    dimensions[0] = imax + 2*ghosts_op;
    dimensions[1] = jmax + 2*ghosts_op;
    dimensions[2] = kmax + 2*ghosts_op;
    ierr = solver->WriteOutput(&dimensions[0],x_op,y_op,z_op,u_op,v_op,w_op,p_op,b_op,filename);
    if (ierr) return(ierr);
    if (!strcmp(solver->op_overwrite,"no"))  ierr = IncrementFilename(solver->op_filename);

    /* Clean up output arrays */
    free(x_op);
    free(y_op);
    free(z_op);
    free(u_op);
    free(v_op);
    free(w_op);
    free(p_op);
    free(b_op);

  }

  return(0);
}
