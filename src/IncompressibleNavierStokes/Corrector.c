#include <arrayfunctions.h>
#include <mpivars.h>
#include <linearsolver.h>
#include <ibcartins3d.h>

/* Function declarations */
int    ApplyBoundaryConditionsPressure  (int*,double*,void*,void*,int,int);
int    ApplyBoundaryConditionsVelocity  (int*,double*,double*,double*,void*,void*,int,int);
int    ComputeDivergence(double*,void*,void*,double*);

int Corrector(void *s,void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  LinearSolver  *LS     = (LinearSolver*) solver->correctorLS;
  int           ierr    = 0, dimensions[7];
  double        norm    = 0;  /* divergence norm */

  int imax    = solver->imax_local;
  int jmax    = solver->jmax_local;
  int kmax    = solver->kmax_local;
  int ghosts  = solver->ghosts;

  /* Apply boundary conditions */
  dimensions[0] = imax;  dimensions[4] = dimensions[0] + 1;
  dimensions[1] = jmax;  dimensions[5] = dimensions[1] + 1;
  dimensions[2] = kmax;  dimensions[6] = dimensions[2] + 1;
  dimensions[3] = ghosts;
  ierr = ApplyBoundaryConditionsVelocity(&dimensions[0],solver->u,solver->v,solver->w,
                                         solver,mpi,0,1);
  if (ierr) return(ierr);

  /* exchange velocity data over MPI boundaries */
  ierr = MPIExchangeBoundaries(imax+1,jmax  ,kmax  ,ghosts,1,0,0,mpi,solver->u); 
  if (ierr) return(ierr);
  ierr = MPIExchangeBoundaries(imax  ,jmax+1,kmax  ,ghosts,0,1,0,mpi,solver->v); 
  if (ierr) return(ierr);
  ierr = MPIExchangeBoundaries(imax  ,jmax  ,kmax+1,ghosts,0,0,1,mpi,solver->w); 
  if (ierr) return(ierr);

  /* compute divergence */
  double *divergence = solver->divergence;
  ierr = ComputeDivergence(divergence,solver,mpi,&norm); if(ierr) return(ierr);
  ierr = Array3DElementMultiply(imax,jmax,kmax,ghosts,0,solver->iblank,divergence); 
  if(ierr) return(ierr);

  /* set initial guess for phi as zero */
  int size_phi = (imax+2*ghosts) * (jmax+2*ghosts) * (kmax+2*ghosts);
  ierr = SetArrayValue(solver->phi,size_phi,0.0); 
  if(ierr) return(ierr);

  /* Solve for phi */
  int pshift[3] = {0,0,0};
  dimensions[0] = imax;
  dimensions[1] = jmax;
  dimensions[2] = kmax;
  dimensions[3] = ghosts;
  ierr = LS->Solver(LS,mpi,solver->phi,divergence,&dimensions[0],&pshift[0],solver->iblank);
  if (ierr) return(ierr);

  /* apply boundary conditions and exchange data across processes for phi */
  ierr = ApplyBoundaryConditionsPressure(&dimensions[0],solver->phi,solver,mpi,0,0); 
  if(ierr) return(ierr);
  ierr = MPIExchangeBoundaries(imax,jmax,kmax,ghosts,0,0,0,mpi,solver->phi);
  if(ierr) return(ierr);

  int is = mpi->is; int ip = mpi->ip; int iproc = mpi->iproc;
  int js = mpi->js; int jp = mpi->jp; int jproc = mpi->jproc;
  int ks = mpi->ks; int kp = mpi->kp; int kproc = mpi->kproc;

  /* Correct the velocity */
  int i,j,k;
  double *phi = solver->phi;
  double *x   = solver->xg;
  double *y   = solver->yg;
  double *z   = solver->zg;
  /* Update u velocity */
  for (i = 0; i < imax+1; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i  ,j,k,imax+1,jmax,kmax,ghosts);
        int pE = Index1D(i  ,j,k,imax  ,jmax,kmax,ghosts);
        int pW = Index1D(i-1,j,k,imax  ,jmax,kmax,ghosts);
        double dxinv;
        if      ((i == 0) && (ip == 0))           dxinv = 1.0 / (x[is+i+1] - x[is+i]  );
        else if ((i == imax) && (ip == iproc-1))  dxinv = 1.0 / (x[is+i-1] - x[is+i-2]);
        else                                      dxinv = 1.0 / (x[is+i]   - x[is+i-1]);
        double phi_x = (phi[pE] - phi[pW]) * dxinv;
        solver->u[p] -= solver->iblanku[p] * phi_x;
      }
    }
  }
  
  /* Update v velocity */
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax+1; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i,j  ,k,imax,jmax+1,kmax,ghosts);
        int pN = Index1D(i,j  ,k,imax,jmax  ,kmax,ghosts);
        int pS = Index1D(i,j-1,k,imax,jmax  ,kmax,ghosts);
        double dyinv;
        if      ((j == 0) && (jp == 0))           dyinv = 1.0 / (y[js+j+1] - y[js+j]  );
        else if ((j == jmax) && (jp == jproc-1))  dyinv = 1.0 / (y[js+j-1] - y[js+j-2]);
        else                                      dyinv = 1.0 / (y[js+j]   - y[js+j-1]);
        double phi_y = (phi[pN] - phi[pS]) * dyinv;
        solver->v[p] -= solver->iblankv[p] * phi_y;
      }
    }
  }
  
  /* Update w velocity */
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax+1; k++) {
        int p  = Index1D(i,j,k  ,imax,jmax,kmax+1,ghosts);
        int pT = Index1D(i,j,k  ,imax,jmax,kmax  ,ghosts);
        int pB = Index1D(i,j,k-1,imax,jmax,kmax  ,ghosts);
        double dzinv;
        if      ((k == 0) && (kp == 0))           dzinv = 1.0 / (z[ks+k+1] - z[ks+k]  );
        else if ((k == kmax) && (kp == kproc-1))  dzinv = 1.0 / (z[ks+k-1] - z[ks+k-2]);
        else                                      dzinv = 1.0 / (z[ks+k]   - z[ks+k-1]);
        double phi_z = (phi[pT] - phi[pB]) * dzinv;
        solver->w[p] -= solver->iblankw[p] * phi_z;
      }
    }
  }
  
  /* compute divergence again and store the norm */
  ierr = ComputeDivergence(divergence,solver,mpi,&norm);
  if(ierr) return(ierr);
  solver->divergence_norm = norm;

  return(0);
}
