#include <stdio.h>
#include <arrayfunctions.h>

/* Calculates the staggered 1D array from a collocated 1D array */
/* a[n+1] from ac[n]                                            */
int InterpolateCollToStag1D(double *ac,double *a,int n)
{
  int i;
  a[0] = 0.5 * (3*ac[0] - ac[1]);
  for (i = 1; i < n; i++) {
    a[i] = 0.5 * (ac[i-1] + ac[i]);
  }
  a[n] = 0.5 * (3*ac[n-1] - ac[n-2]);
  return 0;
}

/* calculates the staggered u,v,w components from collocated data         */
/* These arrays should already be allocated:-                             */
/* uc[imax]  [jmax][kmax],vc[imax][jmax]  [kmax], wc[imax][jmax][kmax]    */
/*  u[imax+1][jmax][kmax], v[imax][jmax+1][kmax],  w[imax][jmax][kmax+1]  */
int InterpolateCollToStag(double *uc,double *vc,double *wc,
                          double *u, double *v, double *w,
                          int imax, int jmax, int kmax, int g)
{
  int i,j,k;

  for (i = 0; i < 1; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i  ,j,k,imax+1,jmax,kmax,g);
        q1 = Index1D(i  ,j,k,imax  ,jmax,kmax,0);
        q2 = Index1D(i+1,j,k,imax  ,jmax,kmax,0);
        u[p] = 0.5 * (3*uc[q1] - uc[q2]);
      }
    }
  }
  for (i = 1; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i  ,j,k,imax+1,jmax,kmax,g);
        q1 = Index1D(i-1,j,k,imax  ,jmax,kmax,0);
        q2 = Index1D(i  ,j,k,imax  ,jmax,kmax,0);
        u[p] = 0.5 * (uc[q1] + uc[q2]);
      }
    }
  }
  for (i = imax; i < imax+1; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i  ,j,k,imax+1,jmax,kmax,g);
        q1 = Index1D(i-1,j,k,imax  ,jmax,kmax,0);
        q2 = Index1D(i-2,j,k,imax  ,jmax,kmax,0);
        u[p] = 0.5 * (3*uc[q1] - uc[q2]);
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (j = 0; j < 1; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i,j  ,k,imax,jmax+1,kmax,g);
        q1 = Index1D(i,j  ,k,imax,jmax  ,kmax,0);
        q2 = Index1D(i,j+1,k,imax,jmax  ,kmax,0);
        v[p] = 0.5 * (3*vc[q1] - vc[q2]);
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 1; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i,j  ,k,imax,jmax+1,kmax,g);
        q1 = Index1D(i,j-1,k,imax,jmax  ,kmax,0);
        q2 = Index1D(i,j  ,k,imax,jmax  ,kmax,0);
        v[p] = 0.5 * (vc[q1] + vc[q2]);
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = jmax; j < jmax+1; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i,j  ,k,imax,jmax+1,kmax,g);
        q1 = Index1D(i,j-1,k,imax,jmax  ,kmax,0);
        q2 = Index1D(i,j-2,k,imax,jmax  ,kmax,0);
        v[p] = 0.5 * (3*vc[q1] - vc[q2]);
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < 1; k++) {
        int p, q1, q2;
        p  = Index1D(i,j,k  ,imax,jmax,kmax+1,g);
        q1 = Index1D(i,j,k  ,imax,jmax,kmax  ,0);
        q2 = Index1D(i,j,k+1,imax,jmax,kmax  ,0);
        w[p] = 0.5 * (3*wc[q1] - wc[q2]);
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 1; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i,j,k  ,imax,jmax,kmax+1,g);
        q1 = Index1D(i,j,k-1,imax,jmax,kmax  ,0);
        q2 = Index1D(i,j,k  ,imax,jmax,kmax  ,0);
        w[p] = 0.5 * (wc[q1] + wc[q2]);
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = kmax; k < kmax+1; k++) {
        int p, q1, q2;
        p  = Index1D(i,j,k  ,imax,jmax,kmax+1,g);
        q1 = Index1D(i,j,k-1,imax,jmax,kmax  ,0);
        q2 = Index1D(i,j,k-2,imax,jmax,kmax  ,0);
        w[p] = 0.5 * (3*wc[q1] - wc[q2]);
      }
    }
  }

  return(0);
}

/* calculates the collocated u,v,w components from staggered data         */
/* These arrays should already be allocated:-                             */
/*  u[imax+1][jmax][kmax], v[imax][jmax+1][kmax],  w[imax][jmax][kmax+1]  */
/* uc[imax]  [jmax][kmax],vc[imax][jmax]  [kmax], wc[imax][jmax][kmax]    */
int InterpolateStagToColl(double *u, double *v, double *w,
                          double *uc,double *vc,double *wc,
                          int imax, int jmax, int kmax, int g,int gc)
{
  int i,j,k;

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i,j,k,imax,jmax,kmax,gc);
        q1 = Index1D(i,  j,k,imax+1,jmax,kmax,g);
        q2 = Index1D(i+1,j,k,imax+1,jmax,kmax,g);
        uc[p] = 0.5 * (u[q1] + u[q2]);
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i,j,k,imax,jmax,kmax,gc);
        q1 = Index1D(i,j  ,k,imax,jmax+1,kmax,g);
        q2 = Index1D(i,j+1,k,imax,jmax+1,kmax,g);
        vc[p] = 0.5 * (v[q1] + v[q2]);
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p, q1, q2;
        p  = Index1D(i,j,k,imax,jmax,kmax,gc);
        q1 = Index1D(i,j,k  ,imax,jmax,kmax+1,g);
        q2 = Index1D(i,j,k+1,imax,jmax,kmax+1,g);
        wc[p] = 0.5 * (w[q1] + w[q2]);
      }
    }
  }

  return(0);
}

int IncrementFilename(char *f)
{
  int ierr = 0;
  if (f[7] == '9') {
    f[7] = '0';
    if (f[6] == '9') {
      f[6] = '0';
      if (f[5] == '9') {
        f[5] = '0';
        if (f[4] == '9') {
          f[4] = '0';
          if (f[3] == '9') {
            f[3] = '0';
            fprintf(stderr,"Warning: file increment hit max limit. Resetting to zero.\n");
            ierr = 1;
          } else {
            f[3]++;
          }
        } else {
          f[4]++;
        }
      } else {
        f[5]++;
      }
    } else {
      f[6]++;
    }
  } else {
    f[7]++;
  }
  return(ierr);
}

