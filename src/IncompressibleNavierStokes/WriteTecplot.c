#include <stdio.h>
#include <string.h>
#include <arrayfunctions.h>
#include <ibcartins3d.h>

int WriteTecplot(int *dim, double *x,double *y,double *z,
                           double *u, double *v, double *w, 
                           double *p, double *b,char *f)
{
  int         i,j,k;

  int imax = dim[0];
  int jmax = dim[1];
  int kmax = dim[2];

  printf("Writing tecplot solution file %s.\n",f);
  FILE *out;
  out = fopen(f,"w");
  if (!out) {
    fprintf(stderr,"Error: could not open %s for writing.\n",f);
    return(1);
  }
  fprintf(out,"VARIABLES=\"I\",\"J\",\"K\",\"X\",\"Y\",\"Z\",\"u\",\"v\",\"w\",\"P\",\"body\"\n");
  fprintf(out,"ZONE I=%d,J=%d,K=%d,F=POINT\n",kmax,jmax,imax);
  for (i = 0; i < imax; i++) { 
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int q = Index1D(i,j,k,imax,jmax,kmax,0);
        fprintf(out,"%4d %4d %4d %lf %lf %lf ",i,j,k,x[i],y[j],z[k]);
        fprintf(out,"%lf %lf %lf %lf %lf\n",u[q],v[q],w[q],p[q],b[q]);
      }
    }
  }
  fclose(out);
  return(0);
}
