#include <stdio.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <timeintegration.h>
#include <linearsolver.h>
#include <ibcartins3d.h>

/* Function declarations */
int Predictor       (void*,void*,double*,double*,double*);
int Corrector       (void*,void*);

int LowStorageRK3(void *ts,void *s,void *m)
{
  TimeIntegration *TS     = (TimeIntegration*) ts;
  IBCartINS3D     *solver = (IBCartINS3D*) s;
  MPIVariables    *mpi    = (MPIVariables*) m;
  LinearSolver    *LSu    = (LinearSolver*) solver->predictorLSu;
  LinearSolver    *LSv    = (LinearSolver*) solver->predictorLSv;
  LinearSolver    *LSw    = (LinearSolver*) solver->predictorLSw;
  LinearSolver    *LSp    = (LinearSolver*) solver->correctorLS;
  int             ierr    = 0;

  /* Low Storage RK3 parameters */
  int stage, nStages = 3;
  double coeff1[3] = {8.0/15.0,  5.0/12.0, 3.0/4.0 };
  double coeff2[3] = {0.0     ,-17.0/60.0,-5.0/12.0};

  int imax    = solver->imax_local;
  int jmax    = solver->jmax_local;
  int kmax    = solver->kmax_local;
  int ghosts  = solver->ghosts;

  /* allocate du, dv, dw */
  int size_u = (imax+1+2*ghosts) * (jmax  +2*ghosts) * (kmax  +2*ghosts);
  int size_v = (imax  +2*ghosts) * (jmax+1+2*ghosts) * (kmax  +2*ghosts);
  int size_w = (imax  +2*ghosts) * (jmax  +2*ghosts) * (kmax+1+2*ghosts);
  double *du, *dv, *dw, *prev_du, *prev_dv, *prev_dw;
  du      = solver->du;
  dv      = solver->dv;
  dw      = solver->dw;
  prev_du = solver->prev_du;
  prev_dv = solver->prev_dv;
  prev_dw = solver->prev_dw;

  for (stage = 0; stage < nStages; stage++) {

    /* Predictor stage: convection and diffusion */
    ierr = Predictor(solver,mpi,du,dv,dw);     if(ierr) return(ierr);
    ierr = ArrayAXPY(du     ,coeff1[stage]*solver->dt,solver->u,size_u); if(ierr) return(ierr);
    ierr = ArrayAXPY(dv     ,coeff1[stage]*solver->dt,solver->v,size_v); if(ierr) return(ierr);
    ierr = ArrayAXPY(dw     ,coeff1[stage]*solver->dt,solver->w,size_w); if(ierr) return(ierr);
    ierr = ArrayAXPY(prev_du,coeff2[stage]*solver->dt,solver->u,size_u); if(ierr) return(ierr);
    ierr = ArrayAXPY(prev_dv,coeff2[stage]*solver->dt,solver->v,size_v); if(ierr) return(ierr);
    ierr = ArrayAXPY(prev_dw,coeff2[stage]*solver->dt,solver->w,size_w); if(ierr) return(ierr);

    /* Copy du,dv,dw to prev_du,prev_dv,prev_dw) */
    ierr = ArrayCopy(du,prev_du,size_u); if(ierr) return(ierr);
    ierr = ArrayCopy(dv,prev_dv,size_v); if(ierr) return(ierr);
    ierr = ArrayCopy(dw,prev_dw,size_w); if(ierr) return(ierr);

    /* Pressure correction */
    ierr = Corrector(solver,mpi); if(ierr) return(ierr);

    /* print linear solver convergence info */
    if  ((!mpi->rank) && ((TS->iter+1)%solver->screen_op_iter == 0) && solver->print_ls) 
      printf("  (iter,norm) -> u: %d,%E  v: %d,%E  w: %d,%E  p: %d,%E\n",
             LSu->exit_iter, LSu->exit_norm, LSv->exit_iter, LSv->exit_norm,
             LSw->exit_iter, LSw->exit_norm, LSp->exit_iter, LSp->exit_norm);
  }

  return(0);
}
