#include <stdio.h>
#include <immersedbody.h>
#include <mathroutines.h>

int IBNearestNormal(int nb,int nf,void *f,void *ib,double *x,double *y,double *z,double large_distance)
{
  facet         *surface  = (facet*) f;
  BoundaryNode  *boundary = (BoundaryNode*) ib;
  double        eps       = IBTolerance;


	int i, j, k, dg, n;
	for (dg = 0; dg < nb; dg++) {
		i = boundary[dg].i;
		j = boundary[dg].j;
		k = boundary[dg].k;
		double  xp = x[i];
		double  yp = y[j];
		double  zp = z[k];

		double  dist_min = large_distance;
		int     n_min    = -1;

		for (n = 0; n < nf; n++) {
			double x1, x2, x3;
			double y1, y2, y3;
			double z1, z2, z3;
			x1 = surface[n].x1;	x2 = surface[n].x2;	x3 = surface[n].x3;
			y1 = surface[n].y1;	y2 = surface[n].y2;	y3 = surface[n].y3;
			z1 = surface[n].z1;	z2 = surface[n].z2;	z3 = surface[n].z3;
			double dist =   surface[n].nx*(xp-surface[n].x1) 
                    + surface[n].ny*(yp-surface[n].y1) 
                    + surface[n].nz*(zp-surface[n].z1);
			if (dist > 0)	continue;
			if (absolute(dist) < dist_min) {
				short   is_it_in = 0;
				double  x_int, y_int, z_int;
				x_int = xp - dist * surface[n].nx;
				y_int = yp - dist * surface[n].ny;
				z_int = zp - dist * surface[n].nz;
				if (absolute(surface[n].nx) > eps) {
					double den = (z2-z3)*(y1-y3)-(y2-y3)*(z1-z3);
					double l1, l2, l3;
					l1 = ((y2-y3)*(z3-z_int)-(z2-z3)*(y3-y_int)) / den;
					l2 = ((z1-z3)*(y3-y_int)-(y1-y3)*(z3-z_int)) / den;
					l3 = 1 - l1 - l2;
					if ((l1 > -eps) && (l2 > -eps) && (l3 > -eps))	is_it_in = 1;
				} else if (absolute(surface[n].ny) > eps) {
					double den = (x2-x3)*(z1-z3)-(z2-z3)*(x1-x3);
					double l1, l2, l3;
					l1 = ((z2-z3)*(x3-x_int)-(x2-x3)*(z3-z_int)) / den;
					l2 = ((x1-x3)*(z3-z_int)-(z1-z3)*(x3-x_int)) / den;
					l3 = 1 - l1 - l2;
					if ((l1 > -eps) && (l2 > -eps) && (l3 > -eps))	is_it_in = 1;
				} else {
					double den = (y2-y3)*(x1-x3)-(x2-x3)*(y1-y3);
					double l1, l2, l3;
					l1 = ((x2-x3)*(y3-y_int)-(y2-y3)*(x3-x_int)) / den;
					l2 = ((y1-y3)*(x3-x_int)-(x1-x3)*(y3-y_int)) / den;
					l3 = 1 - l1 - l2;
					if ((l1 > -eps) && (l2 > -eps) && (l3 > -eps))	is_it_in = 1;
				}
				if (is_it_in) {
					dist_min = absolute(dist);
					n_min = n; 
				}
			}
		}
		if (n_min == -1) {
			for (n = 0; n < nf; n++) {
				double dist =   surface[n].nx*(xp-surface[n].x1) 
                      + surface[n].ny*(yp-surface[n].y1) 
                      + surface[n].nz*(zp-surface[n].z1);
				if (dist > 0)	continue;
				else {
					if (absolute(dist) < dist_min) {
						dist_min = absolute(dist);
						n_min = n;
					}
				}
			}
		}

		if (n_min == -1)	{
      fprintf(stderr,"Error: no nearest normal found for boundary node (%d,%d,%d).\n",i,j,k);
      return(1);
    } else {
      boundary[dg].nx = surface[n_min].nx;
      boundary[dg].ny = surface[n_min].ny;
      boundary[dg].nz = surface[n_min].nz;
      boundary[dg].xv = surface[n_min].x1;
      boundary[dg].yv = surface[n_min].y1;
      boundary[dg].zv = surface[n_min].z1;
    }
	}

  return(0);
}
