#include <arrayfunctions.h>
#include <immersedbody.h>

int IBCountBoundaryPoints(int imax,int jmax,int kmax,int ghosts,double *blank)
{
	int i, j, k;
  int count = 0;
	for (i = 0; i < imax; i++) {
		for (j = 0; j < jmax; j++) {
			for (k = 0; k < kmax; k++) {
				int p = Index1D(i,j,k,imax,jmax,kmax,ghosts);
        /* if this point is inside the body (0), find out if any */
        /* of the neighboring points are outside (1)              */
				if (!blank[p]){
          int g;
          int flag = 0;
					for (g = 1; g <= ghosts; g++){
            int q;

            q = Index1D(i+g,j,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i-g,j,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j+g,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j-g,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j,k+g,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j,k-g,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

					}
          if (flag)   count++;
				}
			}
		}
	}
  return(count);
}
