/* Constants */
const int _REFLECT_   = 0;
const int _EXTRP_     = 1;

/* Parameters */
int       IBIterLimit = 1000;
double IBTolerance    = 1e-12;
