#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <immersedbody.h>

int STLFileRead(char *file,facet **IB,int *nfacets)
{
  int   no_of_facets = 0;;
	char  word[_MAX_STRING_SIZE_];

	std::ifstream in;
	in.open(file);
	if (in) {
    /* Count number of facets in STL file */
		in >> word;
		if (!strcmp(word, "solid")) {
			no_of_facets = 0;
			while (strcmp(word, "endsolid")) {
				in >> word;
				if (!strcmp(word, "facet")) {
					no_of_facets++;
				}
			}
		} else {
			fprintf(stderr,"Error: Illegal keyword read in STL file.\n");
      return(1);
		}
	} else {
		if (*IB)	free(*IB);
    *nfacets = 0;
    return(0);
	}
	in.close();

  /* Allocate array to store STL data */
	if (*IB) {
		fprintf(stderr,"Error: Pointer to immersed body seems to be already allocated(?).\n");
		free(*IB);
	}
	*IB = (facet*) calloc(no_of_facets, sizeof(facet));

  /* Read in the STL data */
	in.open(file);
	in >> word;
	int n = 0;
	while (strcmp(word, "endsolid")) {
		double t1, t2, t3;
		in >> word;
		if (!strcmp(word, "facet")) {
			in >> word;
			if (strcmp(word, "normal")) {
				fprintf(stderr,"Error: Illegal keyword read in %s.\n",file);
			}else {
				in >> t1 >> t2 >> t3;
				(*IB)[n].nx = t1; 
				(*IB)[n].ny = t2; 
				(*IB)[n].nz = t3;
			}
			in >> word;
			if (strcmp(word, "outer")) {
				fprintf(stderr,"Error: Illegal keyword read in %s.\n",file);
			} else {
				in >> word;
				if (strcmp(word, "loop")) {
				  fprintf(stderr,"Error: Illegal keyword read in %s.\n",file);
				} else {
					in >> word;
					if (strcmp(word, "vertex")) {
				    fprintf(stderr,"Error: Illegal keyword read in %s.\n",file);
					} else {
						in >> t1 >> t2 >> t3;
						(*IB)[n].x1 = t1; 
						(*IB)[n].y1 = t2; 
						(*IB)[n].z1 = t3;
					}
					in >> word;
					if (strcmp(word, "vertex")) {
				    fprintf(stderr,"Error: Illegal keyword read in %s.\n",file);
					} else {
						in >> t1 >> t2 >> t3;
						(*IB)[n].x2 = t1; 
						(*IB)[n].y2 = t2; 
						(*IB)[n].z2 = t3;
					}
					in >> word;
					if (strcmp(word, "vertex")) {
				    fprintf(stderr,"Error: Illegal keyword read in %s.\n",file);
					} else {
						in >> t1 >> t2 >> t3;
						(*IB)[n].x3 = t1; 
						(*IB)[n].y3 = t2; 
						(*IB)[n].z3 = t3;
					}
				}
			} 
			n++;
		}
	}
  *nfacets = no_of_facets;
  return(0);
}
