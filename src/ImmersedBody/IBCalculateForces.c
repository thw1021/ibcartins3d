#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <arrayfunctions.h>
#include <mathroutines.h>
#include <mpivars.h>
#include <immersedbody.h>

static int CalculateForces3D(void*,void*,int*,int*,double*,double*,double*,double*,double*,
                               double*,double*,double*,double*,double*,double,double*);

int IBCalculateBodyForces(void *b,void *m,int *grid_dim,int* offset,
                      double *x,double *y,double *z,
                      double *xu,double *yv,double *zw,
                      double *u,double *v,double *w,double *phi,
                      double mu,char *flag2d,double *forces)
{
  int ierr = 0;
  if (!strcmp(flag2d,"no")) 
    ierr = CalculateForces3D(b,m,grid_dim,offset,x,y,z,xu,yv,zw,u,v,w,phi,mu,forces);
  return(ierr);
}

int CalculateForces3D(void *b,void *m,int *grid_dim,int *offset,
                        double *x,double *y,double *z,
                        double *xu,double *yv,double *zw,
                        double *u,double *v,double *w,double *phi,
                        double mu,double *global_force)
{
  ImmersedBody  *body = (ImmersedBody*) b;
  MPIVariables  *mpi  = (MPIVariables*) m;
  int           ierr  = 0, nf,nn;
  double        *surface_pressure;
  double        *shearx, *sheary, *shearz, *shearm;

  int imax    = grid_dim[0];
  int jmax    = grid_dim[1];
  int kmax    = grid_dim[2];
  int ghosts  = grid_dim[3];
  int is      = offset[0];
  int js      = offset[1];
  int ks      = offset[2];

  int istart  = 0;
  int jstart  = 0;
  int kstart  = 0;
  int iend    = (mpi->ip == mpi->iproc-1 ? imax-1 : imax);
  int jend    = (mpi->jp == mpi->jproc-1 ? jmax-1 : jmax);
  int kend    = (mpi->kp == mpi->kproc-1 ? kmax-1 : kmax);

  if (!body->is_present) {
    fprintf(stderr,"Error in IBCalculateForces3D(): Immersed body not present. Do not call this function.\n");
    return(1);
  }

  /* Find out which facets belong to this proc */
	int *this_proc_facets;
	int n_facets_this_proc = 0;
	for (nf = 0; nf < body->nfacets; nf++){
		double xc, yc, zc;
		double x1, y1, z1;
		double x2, y2, z2;
		double x3, y3, z3;

    /* facet corners */
		x1 = body->surface[nf].x1;  y1 = body->surface[nf].y1;  z1 = body->surface[nf].z1;
		x2 = body->surface[nf].x2; 	y2 = body->surface[nf].y2;	z2 = body->surface[nf].z2;
		x3 = body->surface[nf].x3;	y3 = body->surface[nf].y3;	z3 = body->surface[nf].z3;

    /* facet centroid */
		xc = (x1 + x2 + x3) / 3.0;
		yc = (y1 + y2 + y3) / 3.0;
		zc = (z1 + z2 + z3) / 3.0;

    /* check if centroid lies within this process' part of the grid */
		int i, j, k;
    int flag_x, flag_y, flag_z;
    flag_x = 0;
    flag_y = 0;
    flag_z = 0;
		for (i = istart; i < iend; i++) if ((xc-x[is+i])*(xc-x[is+i+1]) <= 0)	flag_x = 1;
		for (j = jstart; j < jend; j++) if ((yc-y[js+j])*(yc-y[js+j+1]) <= 0)	flag_y = 1;
		for (k = kstart; k < kend; k++) if ((zc-z[ks+k])*(zc-z[ks+k+1]) <= 0)	flag_z = 1;
    if (flag_x && flag_y && flag_z) n_facets_this_proc++;
	}

  int total_facets = 0;
  ierr = MPISum_integer(&total_facets,&n_facets_this_proc,1); if(ierr) return(ierr);
  if (!mpi->rank) {
    if (total_facets != body->nfacets) {
      fprintf(stderr,"Error in IBCalculateForces3D(): counting facets on each process failed.\n");
      fprintf(stderr,"Total number of facets = %d, but total from processes = %d.\n",body->nfacets,total_facets);
      return(1);
    }
  }

  /* Create a set of facets which lie within the local domain of this process */
  this_proc_facets = (int*) calloc(n_facets_this_proc, sizeof(int));
	if (n_facets_this_proc > 0)	{
	  nn = 0;
	  for (nf = 0; nf < body->nfacets; nf++) {
  		double xc, yc, zc;
  		double x1, y1, z1;
  		double x2, y2, z2;
  		double x3, y3, z3;
  
      /* facet corners */
  		x1 = body->surface[nf].x1;  y1 = body->surface[nf].y1;  z1 = body->surface[nf].z1;
  		x2 = body->surface[nf].x2; 	y2 = body->surface[nf].y2;	z2 = body->surface[nf].z2;
  		x3 = body->surface[nf].x3;	y3 = body->surface[nf].y3;	z3 = body->surface[nf].z3;
  
      /* facet centroid */
  		xc = (x1 + x2 + x3) / 3.0;
  		yc = (y1 + y2 + y3) / 3.0;
  		zc = (z1 + z2 + z3) / 3.0;
  
      /* check if centroid lies within this process' part of the grid */
  		int i, j, k;
      int flag_x, flag_y, flag_z;
      flag_x = 0;
      flag_y = 0;
      flag_z = 0;
  		for (i = istart; i < iend; i++) if ((xc-x[is+i])*(xc-x[is+i+1]) <= 0)	flag_x = 1;
  		for (j = jstart; j < jend; j++) if ((yc-y[js+j])*(yc-y[js+j+1]) <= 0)	flag_y = 1;
  		for (k = kstart; k < kend; k++) if ((zc-z[ks+k])*(zc-z[ks+k+1]) <= 0)	flag_z = 1;
      if (flag_x && flag_y && flag_z) {
        this_proc_facets[nn] = nf;
        nn++;
      }
  	}
  	if (nn != n_facets_this_proc)	fprintf(stderr,"Error in IBCalculateForces3D(): Inconsistency in facet counting\n");
	}

  /* allocate arrays for the surface pressure and shear forces */
	surface_pressure = (double*) calloc(n_facets_this_proc, sizeof(double));
	shearx = (double*) calloc(n_facets_this_proc, sizeof(double));
	sheary = (double*) calloc(n_facets_this_proc, sizeof(double));
	shearz = (double*) calloc(n_facets_this_proc, sizeof(double));
	shearm = (double*) calloc(n_facets_this_proc, sizeof(double));

	for (nn = 0; nn < n_facets_this_proc; nn++){
		nf = this_proc_facets[nn];
		double xc, yc, zc;
		double x1, y1, z1;
		double x2, y2, z2;
		double x3, y3, z3;

		x1 = body->surface[nf].x1;  y1 = body->surface[nf].y1;  z1 = body->surface[nf].z1;
		x2 = body->surface[nf].x2;  y2 = body->surface[nf].y2;	z2 = body->surface[nf].z2;
		x3 = body->surface[nf].x3;	y3 = body->surface[nf].y3;	z3 = body->surface[nf].z3;

		xc = (x1 + x2 + x3) / 3;
		yc = (y1 + y2 + y3) / 3;
		zc = (z1 + z2 + z3) / 3;

		double nx, ny, nz;
		nx = body->surface[nf].nx;
		ny = body->surface[nf].ny;
		nz = body->surface[nf].nz;

		int     i, j, k, ii, jj, kk;
    double  tlx[2],tly[2],tlz[2],var[8];

    /* find the main grid cell inside with this centroid lies */
		ii = 0;
		jj = 0;
		kk = 0;
		for (i = istart; i < iend; i++) if (x[is+i] < xc)	ii = i;			
		for (j = jstart; j < jend; j++) if (y[js+j] < yc)	jj = j;
		for (k = kstart; k < kend; k++) if (z[ks+k] < zc)	kk = k;
    /* calculating surface pressure at the centroid */
    tlx[0] = x[is+ii]  ; tly[0] = y[js+jj]  ; tlz[0] = z[ks+kk]  ;
    tlx[1] = x[is+ii+1]; tly[1] = y[js+jj+1]; tlz[1] = z[ks+kk+1];
    var[0] = phi[Index1D(i  ,j  ,k  ,imax,jmax,kmax,ghosts)];
    var[1] = phi[Index1D(i+1,j  ,k  ,imax,jmax,kmax,ghosts)];
    var[2] = phi[Index1D(i  ,j+1,k  ,imax,jmax,kmax,ghosts)];
    var[3] = phi[Index1D(i+1,j+1,k  ,imax,jmax,kmax,ghosts)];
    var[4] = phi[Index1D(i  ,j  ,k+1,imax,jmax,kmax,ghosts)];
    var[5] = phi[Index1D(i+1,j  ,k+1,imax,jmax,kmax,ghosts)];
    var[6] = phi[Index1D(i+1,j+1,k+1,imax,jmax,kmax,ghosts)];
    var[7] = phi[Index1D(i+1,j+1,k+1,imax,jmax,kmax,ghosts)];
		surface_pressure[nn] = TrilinearInterpolation(tlx,tly,tlz,var,xc,yc,zc);

    /* calculating velocity derivatives at the surface */
		double dx = x[is+i+1] - x[is+i];
		double dy = y[js+j+1] - y[js+j];
		double dz = z[ks+k+1] - z[ks+k];
		double ds = min3(dx,dy,dz);
    double u_surface,v_surface,w_surface;

    /* near-surface point */
		double xx1 = xc + sign(nx)*ds;
		double yy1 = yc + sign(ny)*ds;
		double zz1 = zc + sign(nz)*ds;
    /* find the u-grid cell containing this point */
		ii = 0;
		jj = 0;
		kk = 0;
		for (i = istart; i < iend+1; i++)  if (xu[is+i] < xx1)	ii = i;			
		for (j = jstart; j < jend  ; j++)  if (y[js+j]  < yy1)	jj = j;
		for (k = kstart; k < kend  ; k++)  if (z[ks+k]  < zz1)	kk = k;
    /* calculating u at the near-surface point */
    tlx[0] = xu[is+ii]  ; tly[0] = y[js+jj]  ; tlz[0] = z[ks+kk]  ;
    tlx[1] = xu[is+ii+1]; tly[1] = y[js+jj+1]; tlz[1] = z[ks+kk+1];
    var[0] = u[Index1D(i  ,j  ,k  ,imax+1,jmax,kmax,ghosts)];
    var[1] = u[Index1D(i+1,j  ,k  ,imax+1,jmax,kmax,ghosts)];
    var[2] = u[Index1D(i  ,j+1,k  ,imax+1,jmax,kmax,ghosts)];
    var[3] = u[Index1D(i+1,j+1,k  ,imax+1,jmax,kmax,ghosts)];
    var[4] = u[Index1D(i  ,j  ,k+1,imax+1,jmax,kmax,ghosts)];
    var[5] = u[Index1D(i+1,j  ,k+1,imax+1,jmax,kmax,ghosts)];
    var[6] = u[Index1D(i+1,j+1,k+1,imax+1,jmax,kmax,ghosts)];
    var[7] = u[Index1D(i+1,j+1,k+1,imax+1,jmax,kmax,ghosts)];
		u_surface = TrilinearInterpolation(tlx,tly,tlz,var,xx1,yy1,zz1);
    /* find the v-grid cell containing this point */
		ii = 0;
		jj = 0;
		kk = 0;
		for (i = istart; i < iend  ; i++)  if (x[is+i]  < xx1)	ii = i;			
		for (j = jstart; j < jend+1; j++)  if (yv[js+j] < yy1)	jj = j;
		for (k = kstart; k < kend  ; k++)  if (z[ks+k]  < zz1)	kk = k;
    /* calculating v at the near-surface point */
    tlx[0] = x[is+ii]  ; tly[0] = yv[js+jj]  ; tlz[0] = z[ks+kk]  ;
    tlx[1] = x[is+ii+1]; tly[1] = yv[js+jj+1]; tlz[1] = z[ks+kk+1];
    var[0] = v[Index1D(i  ,j  ,k  ,imax,jmax+1,kmax,ghosts)];
    var[1] = v[Index1D(i+1,j  ,k  ,imax,jmax+1,kmax,ghosts)];
    var[2] = v[Index1D(i  ,j+1,k  ,imax,jmax+1,kmax,ghosts)];
    var[3] = v[Index1D(i+1,j+1,k  ,imax,jmax+1,kmax,ghosts)];
    var[4] = v[Index1D(i  ,j  ,k+1,imax,jmax+1,kmax,ghosts)];
    var[5] = v[Index1D(i+1,j  ,k+1,imax,jmax+1,kmax,ghosts)];
    var[6] = v[Index1D(i+1,j+1,k+1,imax,jmax+1,kmax,ghosts)];
    var[7] = v[Index1D(i+1,j+1,k+1,imax,jmax+1,kmax,ghosts)];
		v_surface = TrilinearInterpolation(tlx,tly,tlz,var,xx1,yy1,zz1);
    /* find the w-grid cell containing this point */
		ii = 0;
		jj = 0;
		kk = 0;
		for (i = istart; i < iend  ; i++)  if (x[is+i]  < xx1)	ii = i;			
		for (j = jstart; j < jend  ; j++)  if (y[js+j]  < yy1)	jj = j;
		for (k = kstart; k < kend+1; k++)  if (zw[ks+k] < zz1)	kk = k;
    /* calculating w at the near-surface point */
    tlx[0] = x[is+ii]  ; tly[0] = y[js+jj]  ; tlz[0] = zw[ks+kk]  ;
    tlx[1] = x[is+ii+1]; tly[1] = y[js+jj+1]; tlz[1] = zw[ks+kk+1];
    var[0] = w[Index1D(i  ,j  ,k  ,imax,jmax+1,kmax,ghosts)];
    var[1] = w[Index1D(i+1,j  ,k  ,imax,jmax+1,kmax,ghosts)];
    var[2] = w[Index1D(i  ,j+1,k  ,imax,jmax+1,kmax,ghosts)];
    var[3] = w[Index1D(i+1,j+1,k  ,imax,jmax+1,kmax,ghosts)];
    var[4] = w[Index1D(i  ,j  ,k+1,imax,jmax+1,kmax,ghosts)];
    var[5] = w[Index1D(i+1,j  ,k+1,imax,jmax+1,kmax,ghosts)];
    var[6] = w[Index1D(i+1,j+1,k+1,imax,jmax+1,kmax,ghosts)];
    var[7] = w[Index1D(i+1,j+1,k+1,imax,jmax+1,kmax,ghosts)];
		w_surface = TrilinearInterpolation(tlx,tly,tlz,var,xx1,yy1,zz1);
		
		double u_x = u_surface / (sign(nx)*ds);
		double v_x = v_surface / (sign(nx)*ds);
		double w_x = w_surface / (sign(nx)*ds);
		double u_y = u_surface / (sign(ny)*ds);
		double v_y = v_surface / (sign(ny)*ds);
		double w_y = w_surface / (sign(ny)*ds);
		double u_z = u_surface / (sign(nz)*ds);
		double v_z = v_surface / (sign(nz)*ds);
		double w_z = w_surface / (sign(nz)*ds);

		/* calculate shear forces */
		double taux, tauy, tauz;
		taux = mu * (2*u_x*nx + (u_y+v_x)*ny + (u_z+w_x)*nz);
		tauy = mu * ((v_x+u_y)*nx + 2*v_y*ny + (v_z+w_y)*nz);
		tauz = mu * ((w_x+u_z)*nx + (w_y+v_z)*ny + 2*w_z*nz);

		shearx[nn] = taux;
		sheary[nn] = tauy;
		shearz[nn] = tauz;
		shearm[nn] = sqrt(taux*taux + tauy*tauy + tauz*tauz);
	}

/*
	if ((iter+1)%N_fw == 0){
		write_surface_data(n_facets_this_proc, this_proc_facets, surface_pressure, shearx, sheary, shearz, shearm);
	}
*/

  /* calculate total surface forces */
	double Pressure_Force_x = 0;
	double Pressure_Force_y = 0;
	double Pressure_Force_z = 0;
	double Shear_Force_x = 0;
	double Shear_Force_y = 0;
	double Shear_Force_z = 0;

	for (nn = 0; nn < n_facets_this_proc; nn++){
		nf = this_proc_facets[nn];
		double x1, y1, z1;
		double x2, y2, z2;
		double x3, y3, z3;

		x1 = body->surface[nf].x1;  y1 = body->surface[nf].y1;  z1 = body->surface[nf].z1;
		x2 = body->surface[nf].x2;	y2 = body->surface[nf].y2;	z2 = body->surface[nf].z2;
		x3 = body->surface[nf].x3;	y3 = body->surface[nf].y3;	z3 = body->surface[nf].z3;

		double area = TriangleArea(x1,y1,z1,x2,y2,z2,x3,y3,z3);

		double nx, ny, nz;
		nx = body->surface[nf].nx;
		ny = body->surface[nf].ny;
		nz = body->surface[nf].nz;

		double normal_force = surface_pressure[nn] * area;
		double fx, fy, fz;
		fx = -normal_force * nx;
		fy = -normal_force * ny;
		fz = -normal_force * nz;

		Pressure_Force_x += fx;
		Pressure_Force_y += fy;
		Pressure_Force_z += fz;

		Shear_Force_x += shearx[nn] * area;
		Shear_Force_y += sheary[nn] * area;
		Shear_Force_z += shearz[nn] * area;
	}

	double Total_Force_x = Pressure_Force_x + Shear_Force_x;
	double Total_Force_y = Pressure_Force_y + Shear_Force_y;
	double Total_Force_z = Pressure_Force_z + Shear_Force_z;

	double force[9];
	force[0] = Pressure_Force_x;
	force[1] = Pressure_Force_y;
	force[2] = Pressure_Force_z;
	force[3] = Shear_Force_x;
	force[4] = Shear_Force_y;
	force[5] = Shear_Force_z;
	force[6] = Total_Force_x;
	force[7] = Total_Force_y;
	force[8] = Total_Force_z;

  ierr = MPISum_double(&global_force[0],&force[0],9); if(ierr) return(ierr);

  /* deallocate arrays */
	if (surface_pressure) free(surface_pressure);
	if (shearx)           free(shearx);
	if (sheary)           free(sheary);
	if (shearz)           free(shearz);
	if (shearm)           free(shearm);
	if (this_proc_facets) free(this_proc_facets);
  return(0);
}
