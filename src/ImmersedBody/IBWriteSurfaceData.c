#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <mathroutines.h>
#include <mpivars.h>
#include <ibcartins3d.h>


void WriteSurfaceData(int nfacets, int *facets, double *sp, double *tx, double *ty, double *tz, double *tm){
	if (!PID){
		std::cout << "Domain " << Domain_ID << ": Writing immersed body surface forces data... \n";
		double *sp_wd = (double*) calloc (no_of_facets, sizeof(double));
		double *tx_wd = (double*) calloc (no_of_facets, sizeof(double));
		double *ty_wd = (double*) calloc (no_of_facets, sizeof(double));
		double *tz_wd = (double*) calloc (no_of_facets, sizeof(double));
		double *tm_wd = (double*) calloc (no_of_facets, sizeof(double));
		int *check = (int*) calloc (no_of_facets, sizeof(int));
		int nn;
		int check_total_facets = 0;
		for (nn = 0; nn < no_of_facets; nn++){
			check[nn] = 0;
		}
		for (nn = 0; nn < nfacets; nn++){
			if (!check[facets[nn]]){
				sp_wd[facets[nn]] = sp[nn];
				tx_wd[facets[nn]] = tx[nn];
				ty_wd[facets[nn]] = ty[nn];
				tz_wd[facets[nn]] = tz[nn];
				tm_wd[facets[nn]] = tm[nn];
				check[facets[nn]] = 1;
			}else{
				std::cout << "Domain " << Domain_ID << ": Error: Facet " << facets[nn] << " has already been assigned a value. Double counting of facet!\n";
			}
		}
		check_total_facets += nfacets;
		for (int proc = 1; proc < NPROC; proc++){
			int nf_incoming;
			int *facets_incoming;
			double *sp_incoming;
			double *tx_incoming, *ty_incoming, *tz_incoming, *tm_incoming;
#ifndef serial
			MPI_Recv(&nf_incoming, 1, MPI_INT, proc, 98927, MPI_COMM_WORLD, &status);
#endif
			check_total_facets += nf_incoming;
			facets_incoming = (int*) calloc(nf_incoming, sizeof(int));
			sp_incoming = (double*) calloc(nf_incoming, sizeof(double));
			tx_incoming = (double*) calloc(nf_incoming, sizeof(double));
			ty_incoming = (double*) calloc(nf_incoming, sizeof(double));
			tz_incoming = (double*) calloc(nf_incoming, sizeof(double));
			tm_incoming = (double*) calloc(nf_incoming, sizeof(double));
#ifndef serial
			MPI_Recv(facets_incoming, nf_incoming, MPI_INT, proc, 98928, MPI_COMM_WORLD, &status);
			MPI_Recv(sp_incoming, nf_incoming, MPI_DOUBLE, proc, 98929, MPI_COMM_WORLD, &status);
			MPI_Recv(tx_incoming, nf_incoming, MPI_DOUBLE, proc, 98930, MPI_COMM_WORLD, &status);
			MPI_Recv(ty_incoming, nf_incoming, MPI_DOUBLE, proc, 98931, MPI_COMM_WORLD, &status);
			MPI_Recv(tz_incoming, nf_incoming, MPI_DOUBLE, proc, 98932, MPI_COMM_WORLD, &status);
			MPI_Recv(tm_incoming, nf_incoming, MPI_DOUBLE, proc, 98933, MPI_COMM_WORLD, &status);
#endif
			for (nn = 0; nn < nf_incoming; nn++){
				if (!check[facets_incoming[nn]]){
					sp_wd[facets_incoming[nn]] = sp_incoming[nn];
					tx_wd[facets_incoming[nn]] = tx_incoming[nn];
					ty_wd[facets_incoming[nn]] = ty_incoming[nn];
					tz_wd[facets_incoming[nn]] = tz_incoming[nn];
					tm_wd[facets_incoming[nn]] = tm_incoming[nn];
					check[facets_incoming[nn]] = 1;
				}else{
					std::cout << "Domain " << Domain_ID << ": Error: Facet " << facets_incoming[nn] << " has already been assigned a value. Double counting of facet!\n";
				}
			}
			if (facets_incoming)	free(facets_incoming);
			if (sp_incoming)	free(sp_incoming);	
			if (tx_incoming)	free(tx_incoming);	
			if (ty_incoming)	free(ty_incoming);	
			if (tz_incoming)	free(tz_incoming);	
			if (tm_incoming)	free(tm_incoming);	
		}
		if (check_total_facets != no_of_facets)	std::cout << "Domain " << Domain_ID << ": Error: in total facet count (write_surface_data).\n";

		int n;
		std::ofstream out;
		out.open(surface_filename);
		out << "ply\n";
		out << "format ascii 1.0\n";
		out << "comment Surface Pressure data for Immersed Body\n";
		out << "element vertex " << 3*no_of_facets << "\n";
		out << "property float x\n";
		out << "property float y\n";
		out << "property float z\n";
		out << "property float Surface_Pressure\n";
		out << "property float Shear_Force_X\n";
		out << "property float Shear_Force_Y\n";
		out << "property float Shear_Force_Z\n";
		out << "property float Shear_Magnitude\n";
		out << "element face " << no_of_facets << "\n";
		out << "property list uchar int vertex_indices\n";
		out << "end_header\n";
		for (n = 0; n < no_of_facets; n++){
			out << (float) IB[n].x1 << " " << (float) IB[n].y1 << " " << (float) IB[n].z1 << " " << (float) sp_wd[n] << " ";
			out << (float) tx_wd[n] << " " << (float) ty_wd[n] << " " << (float) tz_wd[n] << " " << (float) tm_wd[n] << "\n";
			out << (float) IB[n].x2 << " " << (float) IB[n].y2 << " " << (float) IB[n].z2 << " " << (float) sp_wd[n] << " ";
			out << (float) tx_wd[n] << " " << (float) ty_wd[n] << " " << (float) tz_wd[n] << " " << (float) tm_wd[n] << "\n";
			out << (float) IB[n].x3 << " " << (float) IB[n].y3 << " " << (float) IB[n].z3 << " " << (float) sp_wd[n] << " ";
			out << (float) tx_wd[n] << " " << (float) ty_wd[n] << " " << (float) tz_wd[n] << " " << (float) tm_wd[n] << "\n";
		}
		for (n = 0; n < no_of_facets; n++){
			out << "3 " << 3*n << " " << 3*n+1 << " " << 3*n+2 << "\n";
		}
		out.close();
		free(sp_wd);
		free(tx_wd);
		free(ty_wd);
		free(tz_wd);
		free(tm_wd);
    free(check);
	}else{
#ifndef serial
		MPI_Send(&nfacets, 1, MPI_INT, 0, 98927, MPI_COMM_WORLD);
		MPI_Send(facets, nfacets, MPI_INT, 0, 98928, MPI_COMM_WORLD);
		MPI_Send(sp, nfacets, MPI_DOUBLE, 0, 98929, MPI_COMM_WORLD);
		MPI_Send(tx, nfacets, MPI_DOUBLE, 0, 98930, MPI_COMM_WORLD);
		MPI_Send(ty, nfacets, MPI_DOUBLE, 0, 98931, MPI_COMM_WORLD);
		MPI_Send(tz, nfacets, MPI_DOUBLE, 0, 98932, MPI_COMM_WORLD);
		MPI_Send(tm, nfacets, MPI_DOUBLE, 0, 98933, MPI_COMM_WORLD);
#else
		std::cout << "ERROR: Should NOT be here for a serial compiled code. BUG!\n";
#endif
	}
}
