Unsteady incompressible Navier-Stokes solver based on the Fractional
Step algorithm (Kim & Moin, 1982)
- 3D Cartesian
- Spatial discretization of convective terms:
  + First order upwind
  + Second order central
  + Fourth order central
  + Fifth order upwind (with WENO limiting)
- Time integration
  + Low Storage RK3 for convective terms
  + Trapezoidal for viscous terms
- Additional features
  + Option to discretize flux in conservative or non-conservative forms
  + Immersed boundary methodology to treat solid objects in flow
    (STL geometry needed as input)

-------------------------------------------------------------------------------------

COMPILING:-

If obtained by cloning the GIT/SVN repository, run these commands:-
> autoreconf -i

This will generate the required files for:
> [CFLAGS="..."] ./configure [--with-mpidir=/path/to/mpi] [--prefix=/install/dir]
> make
> make install

If unpacked from tarball, then proceed with ./configure, make and
make install.

-------------------------------------------------------------------------------------

INPUT FILES:-

In general, the code needs the following input files:
+ main.inp            -> general inputs
+ solver.inp          -> solver inputs
+ bc.inp              -> boundary conditions
+ init.dat            -> ASCII data file containing initial solution and grid
+ body.stl            -> STL file describing the geometry of any
                         immersed body. If this file is not present,
                         solver will assume no immersed bodies exist.

The files "init.dat" is an ASCII data file which is generated using a code, so 
that it is in the required format. The sample run directories have sample codes 
to generate it. They can be modified to generate different initial conditions.

-------------------------------------------------------------------------------------

OUTPUT FILES:-

In general, the following output files will be generated:-
+ res.log         -> Time history of residuals
+ op*****         -> Solution output in the format specified in solver.inp
                     (Variables: i,j,k,x,y,z,u,v,w,p,body)
If immersed body present,
+ forces.dat      -> Time history of integrated body forces
+ surface.ply     -> immersed body surface with pressure and shear forces
                     distribution (polygon file) 
                     (http://paulbourke.net/dataformats/ply/)

(**) Note: all input and output filenames except "main.inp" are flexible and 
can be specified in "main.inp".

-------------------------------------------------------------------------------------

SAMPLE RUN CASES:-

The directory $(top_srcdir)/run contains some sample run directories. 

To run a case:-
1. Make a copy of the desired sample run directory.
2. Go to the subdirectory "aux" and compile the code "init.C" (g++)
3. In the run directory, run the executable obtained from step 2. (**)
   This will generate "init.dat", with the grid and initial solution.
4. Run the code
   > /path/to/mpi/bin/mpiexec -n ${NPROC} ${ibcartins3d_binary}
   where NPROC needs to be equal to iproc*jproc*kproc specified in
   "solver.inp".

(**) The executable generated from "init.C" reads the grid size from
     "solver.inp". So re-run it if grid size changes.
     Step 3 make take a while to run in some cases, especially for flows 
     with vortical structures as the initial solution.

Alternatively, one write one's own code to generate an grid + initial solution
file. The required format can be figured out from examining "init.C".

-------------------------------------------------------------------------------------

