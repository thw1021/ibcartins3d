#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define PI 3.141592654

int ITER = 1000;
double alpha = 0.9;

int NI, NJ, NK;
double Lx, Ly, Lz;
double *u, *v, *w;
double *x, *y, *z;
double *vort_x, *vort_y, *vort_z, *vort_magn;
double dx, dy, dz;
double *Lc, *Ls, *Ln, *Le, *Lw, *Lt, *Lb;
double *Uc, *Us, *Un, *Ue, *Uw, *Ut, *Ub;

int c_m(int i, int j, int k){
	return NJ*NK*i + NK*j + k;
}

void inp(){
        std::ifstream in;
        std::cout << "Reading file \"solver.inp\"...\n";
        in.open("solver.inp");
        if (!in){
                std::cout << "Error: Input file \"solver.inp\" not found. Default values will be used.\n";
        }else{
                char word[20];
                in >> word;
                if (!strcmp(word, "begin")){
                        while (strcmp(word, "end")){
                                in >> word;
                                if (!strcmp(word, "imax"))                      in >> NI;
                                else if (!strcmp(word, "jmax"))                 in >> NJ;
                                else if (!strcmp(word, "kmax"))                 in >> NK;
                        }
                }else{ 
                        std::cout << "Error: Illegal format in solver.inp. Crash and burn!\n";
                }
        }
        in.close();
	std::cout << "Grid:\t\t\t" << NI << " X " << NJ << " X " << NK << "\n";
}

void init(){
	x = (double*) calloc(NI*NJ*NK, sizeof(double));
	y = (double*) calloc(NI*NJ*NK, sizeof(double));
	z = (double*) calloc(NI*NJ*NK, sizeof(double));
	u = (double*) calloc(NI*NJ*NK, sizeof(double));
	v = (double*) calloc(NI*NJ*NK, sizeof(double));
	w = (double*) calloc(NI*NJ*NK, sizeof(double));
	vort_x = (double*) calloc(NI*NJ*NK, sizeof(double));
	vort_y = (double*) calloc(NI*NJ*NK, sizeof(double));
	vort_z = (double*) calloc(NI*NJ*NK, sizeof(double));
	vort_magn = (double*) calloc(NI*NJ*NK, sizeof(double));
}

void alloc_coeff(){
	Lc = (double*) calloc(NI*NJ*NK, sizeof(double));
	Ls = (double*) calloc(NI*NJ*NK, sizeof(double));
	Ln = (double*) calloc(NI*NJ*NK, sizeof(double));
	Le = (double*) calloc(NI*NJ*NK, sizeof(double));
	Lw = (double*) calloc(NI*NJ*NK, sizeof(double));
	Lt = (double*) calloc(NI*NJ*NK, sizeof(double));
	Lb = (double*) calloc(NI*NJ*NK, sizeof(double));
	Uc = (double*) calloc(NI*NJ*NK, sizeof(double));
	Us = (double*) calloc(NI*NJ*NK, sizeof(double));
	Un = (double*) calloc(NI*NJ*NK, sizeof(double));
	Ue = (double*) calloc(NI*NJ*NK, sizeof(double));
	Uw = (double*) calloc(NI*NJ*NK, sizeof(double));
	Ut = (double*) calloc(NI*NJ*NK, sizeof(double));
	Ub = (double*) calloc(NI*NJ*NK, sizeof(double));

}

void free_coeff(){
	free(Lc);
	free(Ls);
	free(Ln);
	free(Le);
	free(Lw);
	free(Lt);
	free(Lb);
	free(Uc);
	free(Us);
	free(Un);
	free(Ue);
	free(Uw);
	free(Ut);
	free(Ub);

}

void grid(){
	Lx = 2 * PI;
	Ly = 2 * PI;
	Lz = 2 * PI;

	dx = Lx / ((double)NI);
	dy = Ly / ((double)NJ);
	dz = Lz / ((double)NK);

	for (int i = 0; i < NI; i++){
		for (int j = 0; j < NJ; j++){
			for (int k = 0; k < NK; k++){
				x[c_m(i,j,k)] = -PI + i * dx;
				y[c_m(i,j,k)] = -PI + j * dy;
				z[c_m(i,j,k)] = -PI + k * dz;
			}
		}
	}
}

void set_vort(){
	double D = 1.83;
	double R = 0.491;
	double a = 0.196;
	double w0 = 23.8;
	double eps = 1e-6;

	std::ofstream out;
	out.open("vort.dat");
        out << "VARIABLES=\"I\",\"J\",\"K\",\"X\",\"Y\",\"Z\",\"vort_x\",\"vort_y\",\"vort_z\",\"Vort_magn\"\n";
	out << "ZONE I=" << NK << ",J=" << NJ << ",K=" << NI << ",F=POINT\n";

	double Xc, Yc;
	for (int i = 0; i < NI; i++){
		for (int j = 0; j < NJ; j++){
			for (int k = 0; k < NK; k++){
				int p = c_m(i,j,k);
				vort_x[p] = 0;
				vort_y[p] = 0;
				vort_z[p] = 0;
			}
		}
	}
	Xc = D / sqrt(8);
	Yc = D / sqrt(8);
	for (int i = 0; i < NI; i++){
		for (int j = 0; j < NJ; j++){
			for (int k = 0; k < NK; k++){
				int p = c_m(i,j,k);
				double hyp = sqrt((x[p]-Xc)*(x[p]-Xc) + (y[p]-Yc)*(y[p]-Yc));
				double sin = (y[p] - Yc + eps/sqrt(2)) / (hyp + eps);
				double cos = (x[p] - Xc + eps/sqrt(2)) / (hyp + eps);
				double X, Y;
				X = R * cos + Xc;
				Y = R * sin + Yc;
				double rsq = (x[p]-X)*(x[p]-X) + (y[p]-Y)*(y[p]-Y) + z[p]*z[p];
				double w = w0 * exp(- rsq / (a*a));
				vort_x[p] += - w * sin;
				vort_y[p] += w * cos;
				vort_z[p] += 0;
			}
		}
	}
	Xc = - D / sqrt(8);
	Yc = - D / sqrt(8);
	for (int i = 0; i < NI; i++){
		for (int j = 0; j < NJ; j++){
			for (int k = 0; k < NK; k++){
				int p = c_m(i,j,k);
				double hyp = sqrt((x[p]-Xc)*(x[p]-Xc) + (y[p]-Yc)*(y[p]-Yc));
				double sin = (y[p] - Yc + eps/sqrt(2)) / (hyp + eps);
				double cos = (x[p] - Xc + eps/sqrt(2)) / (hyp + eps);
				double X, Y;
				X = R * cos + Xc;
				Y = R * sin + Yc;
				double rsq = (x[p]-X)*(x[p]-X) + (y[p]-Y)*(y[p]-Y) + z[p]*z[p];
				double w = w0 * exp(- rsq / (a*a));
				vort_x[p] += - w * sin;
				vort_y[p] += w * cos;
				vort_z[p] += 0;
			}
		}
	}
	for (int i = 0; i < NI; i++){
		for (int j = 0; j < NJ; j++){
			for (int k = 0; k < NK; k++){
				int p = c_m(i,j,k);
				vort_magn[p] = sqrt(vort_x[p]*vort_x[p] + vort_y[p]*vort_y[p] + vort_z[p]*vort_z[p]);
				out << i << " " << j << " " << k << "\t";
				out << x[p] << " " << y[p] << " " << z[p] << "\t";
				out << vort_x[p] << " " << vort_y[p] << " " << vort_z[p] << "\t";
				out << vort_magn[p] << "\n";
			}
		}
	}
	out.close();
}

void solve_vel(){
	int i, j, k, iter;
	double *du, *du_, *dv, *dv_, *dw, *dw_;
	double *RES_u, *RES_v, *RES_w;
	double *RHS_u, *RHS_v, *RHS_w;
	du = (double*) calloc(NI*NJ*NK, sizeof(double));
	du_ = (double*) calloc(NI*NJ*NK, sizeof(double));
	dv = (double*) calloc(NI*NJ*NK, sizeof(double));
	dv_ = (double*) calloc(NI*NJ*NK, sizeof(double));
	dw = (double*) calloc(NI*NJ*NK, sizeof(double));
	dw_ = (double*) calloc(NI*NJ*NK, sizeof(double));
	RES_u = (double*) calloc(NI*NJ*NK, sizeof(double));
	RES_v = (double*) calloc(NI*NJ*NK, sizeof(double));
	RES_w = (double*) calloc(NI*NJ*NK, sizeof(double));
	RHS_u = (double*) calloc(NI*NJ*NK, sizeof(double));
	RHS_v = (double*) calloc(NI*NJ*NK, sizeof(double));
	RHS_w = (double*) calloc(NI*NJ*NK, sizeof(double));

	alloc_coeff();
	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int pC = c_m(i, j, k);
				Lb[pC] = 0;
				Ls[pC] = 0;
				Lw[pC] = 0;
				Le[pC] = 0;
				Ln[pC] = 0;
				Lt[pC] = 0;
				Lc[pC] = 0;
				Ub[pC] = 0;
				Us[pC] = 0;
				Uw[pC] = 0;
				Ue[pC] = 0;
				Un[pC] = 0;
				Ut[pC] = 0;
				Uc[pC] = 0;
			}
		}
	}

	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int pC, pB, pS, pW, pE, pN, pT;
				pC = c_m(i, j, k);
				if (k < NK-1)	pT = c_m(i, j, k+1);
				else		pT = c_m(i, j, 0);
				if (k > 0)	pB = c_m(i, j, k-1);
				else		pB = c_m(i, j, NK-1);
				if (j > 0)	pS = c_m(i, j-1, k);
				else		pS = c_m(i, NJ-1, k);
				if (j < NJ-1)	pN = c_m(i, j+1, k);
				else		pN = c_m(i, 0, k);
				if (i > 0)	pW = c_m(i-1, j, k);
				else		pW = c_m(NI-1, j, k);
				if (i < NI-1)	pE = c_m(i+1, j, k);
				else		pE = c_m(0, j, k);

				double Ac, As, An, Ae, Aw, At, Ab;
				double trm1, trm2, trm3;

				Ab = 1 / (dz * dz);
				As = 1 / (dy * dy);
				Aw = 1 / (dx * dx);
				Ae = 1 / (dx * dx);
				An = 1 / (dy * dy);
				At = 1 / (dz * dz);
				Ac =  - (Ab + As + Aw + Ae + An + At);

				Ls[pC] = As / (1 + alpha*Ue[pS] + alpha*Ut[pS]);
				Lw[pC] = Aw / (1 + alpha*Un[pW] + alpha*Ut[pW]);
				Lb[pC] = Ab / (1 + alpha*Ue[pB] + alpha*Un[pB]);

				trm1 = alpha * (Lb[pC]*Un[pB] + Lw[pC]*Un[pW]);
				trm2 = alpha * (Lb[pC]*Ue[pB] + Ls[pC]*Ue[pS]);
				trm3 = alpha * (Lw[pC]*Ut[pW] + Ls[pC]*Ut[pS]);

				Lc[pC] = Ac + trm1 + trm2 + trm3;
				Lc[pC] -= (Lb[pC] * Ut[pB]);
				Lc[pC] -= (Lw[pC] * Ue[pW]);
				Lc[pC] -= (Ls[pC] * Un[pS]);

				Ut[pC] = (At - trm3) / Lc[pC];
				Un[pC] = (An - trm1) / Lc[pC];
				Ue[pC] = (Ae - trm2) / Lc[pC];
			}
		}
	}

	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int pC, pB, pS, pW, pE, pN, pT;
				pC = c_m(i, j, k);
				if (k < NK-1)	pT = c_m(i, j, k+1);
				else		pT = c_m(i, j, 0);
				if (k > 0)	pB = c_m(i, j, k-1);
				else		pB = c_m(i, j, NK-1);
				if (j > 0)	pS = c_m(i, j-1, k);
				else		pS = c_m(i, NJ-1, k);
				if (j < NJ-1)	pN = c_m(i, j+1, k);
				else		pN = c_m(i, 0, k);
				if (i > 0)	pW = c_m(i-1, j, k);
				else		pW = c_m(NI-1, j, k);
				if (i < NI-1)	pE = c_m(i+1, j, k);
				else		pE = c_m(0, j, k);

				double vorty_z, vortz_y;
				vorty_z = (vort_y[pT] - vort_y[pB]) / (2*dz);
				vortz_y = (vort_z[pN] - vort_z[pS]) / (2*dy);
				RHS_u[pC] = vorty_z - vortz_y;

				double vortz_x, vortx_z;
				vortz_x = (vort_z[pE] - vort_z[pW]) / (2*dx);
				vortx_z = (vort_x[pT] - vort_x[pB]) / (2*dz);
				RHS_v[pC] = vortz_x - vortx_z;

				double vortx_y, vorty_x;
				vortx_y = (vort_x[pN] - vort_x[pS]) / (2*dy);
				vorty_x = (vort_y[pE] - vort_y[pW]) / (2*dx);
				RHS_w[pC] = vortx_y - vorty_x;
			}
		}
	}

	for (iter = 0; iter < ITER; iter++){
		for (i = 0; i < NI; i++){
			for (j = 0; j < NJ; j++){
				for (k = 0; k < NK; k++){
					int q = c_m(i, j, k);
					du[q] = 0;
					du_[q] = 0;
					dv[q] = 0;
					dv_[q] = 0;
					dw[q] = 0;
					dw_[q] = 0;
				}
			}
		}
		double res_u = 0;
		double res_v = 0;
		double res_w = 0;
		for (i = 0; i < NI; i++){
			for (j = 0; j < NJ; j++){
				for (k = 0; k < NK; k++){
					int qC, qN, qS, qE, qW, qT, qB;
					qC = c_m(i, j, k);
					if (k < NK-1)	qT = c_m(i, j, k+1);
					else		qT = c_m(i, j, 0);
					if (k > 0)	qB = c_m(i, j, k-1);
					else		qB = c_m(i, j, NK-1);
					if (j > 0)	qS = c_m(i, j-1, k);
					else		qS = c_m(i, NJ-1, k);
					if (j < NJ-1)	qN = c_m(i, j+1, k);
					else		qN = c_m(i, 0, k);
					if (i > 0)	qW = c_m(i-1, j, k);
					else		qW = c_m(NI-1, j, k);
					if (i < NI-1)	qE = c_m(i+1, j, k);
					else		qE = c_m(0, j, k);

					double Ab, As, Aw, Ae, An, At, Ac;
					Ab = 1 / (dz * dz);
					As = 1 / (dy * dy);
					Aw = 1 / (dx * dx);
					Ae = 1 / (dx * dx);
					An = 1 / (dy * dy);
					At = 1 / (dz * dz);
					Ac =  - (Ab + As + Aw + Ae + An + At);
		
					RES_u[qC] = RHS_u[qC];
					RES_u[qC] -= Ae * u[qE];
					RES_u[qC] -= Aw * u[qW];
					RES_u[qC] -= An * u[qN];
					RES_u[qC] -= As * u[qS];
					RES_u[qC] -= At * u[qT];
					RES_u[qC] -= Ab * u[qB];
					RES_u[qC] -= Ac * u[qC];

					RES_v[qC] = RHS_v[qC];
					RES_v[qC] -= Ae * v[qE];
					RES_v[qC] -= Aw * v[qW];
					RES_v[qC] -= An * v[qN];
					RES_v[qC] -= As * v[qS];
					RES_v[qC] -= At * v[qT];
					RES_v[qC] -= Ab * v[qB];
					RES_v[qC] -= Ac * v[qC];

					RES_w[qC] = RHS_w[qC];
					RES_w[qC] -= Ae * w[qE];
					RES_w[qC] -= Aw * w[qW];
					RES_w[qC] -= An * w[qN];
					RES_w[qC] -= As * w[qS];
					RES_w[qC] -= At * w[qT];
					RES_w[qC] -= Ab * w[qB];
					RES_w[qC] -= Ac * w[qC];

					res_u += RES_u[qC] * RES_u[qC];
					res_v += RES_v[qC] * RES_v[qC];
					res_w += RES_w[qC] * RES_w[qC];
				}
			}
		}
		res_u /= (NI*NJ*NK);
		res_v /= (NI*NJ*NK);
		res_w /= (NI*NJ*NK);
		if ((iter+1)%10 == 0){
			std::cout << "iter=" << iter+1 << "\tres_u=" << res_u << "\t";
			std::cout << "res_v=" << res_v << "\tres_w=" << res_w << "\n";
		}
		if (res_u+res_v+res_w < 1e-16){
			break;
		}

		for (i = 0; i < NI; i++){
			for (j = 0; j < NJ; j++){
				for (k = 0; k < NK; k++){
					int qC, qS, qW, qB;
					qC = c_m(i, j, k);
					if (i > 0)	qW = c_m(i-1, j, k);
					else		qW = c_m(NI-1, j, k);
					if (j > 0)	qS = c_m(i, j-1, k);
					else		qS = c_m(i, NJ-1, k);
					if (k > 0)	qB = c_m(i, j, k-1);
					else		qB = c_m(i, j, NK-1);
					
					double trm;
	
					trm = RES_u[qC];
					trm -= Ls[qC] * du_[qS];
					trm -= Lw[qC] * du_[qW];
					trm -= Lb[qC] * du_[qB];
					du_[qC] = trm / Lc[qC];
		
					trm = RES_v[qC];
					trm -= Ls[qC] * dv_[qS];
					trm -= Lw[qC] * dv_[qW];
					trm -= Lb[qC] * dv_[qB];
					dv_[qC] = trm / Lc[qC];
		
					trm = RES_w[qC];
					trm -= Ls[qC] * dw_[qS];
					trm -= Lw[qC] * dw_[qW];
					trm -= Lb[qC] * dw_[qB];
					dw_[qC] = trm / Lc[qC];
				}
			}
		}
		for (i = NI-1; i > -1 ; i--){
			for (j = NJ-1; j > -1; j--){
				for (k = NK-1; k > -1; k--){
					int qC, qN, qE, qT;
					qC = c_m(i, j, k);
					if (i < NI-1)	qE = c_m(i+1, j, k);
					else		qE = c_m(0, j, k);
					if (j < NJ-1)	qN = c_m(i, j+1, k);
					else		qN = c_m(i, 0, k);
					if (k < NK-1)	qT = c_m(i, j, k+1);
					else		qT = c_m(i, j, 0);
					
					du[qC] = du_[qC];
					du[qC] -= Un[qC] * du[qN];
					du[qC] -= Ue[qC] * du[qE];
					du[qC] -= Ut[qC] * du[qT];
					u[qC] += du[qC];
					
					dv[qC] = dv_[qC];
					dv[qC] -= Un[qC] * dv[qN];
					dv[qC] -= Ue[qC] * dv[qE];
					dv[qC] -= Ut[qC] * dv[qT];
					v[qC] += dv[qC];
					
					dw[qC] = dw_[qC];
					dw[qC] -= Un[qC] * dw[qN];
					dw[qC] -= Ue[qC] * dw[qE];
					dw[qC] -= Ut[qC] * dw[qT];
					w[qC] += dw[qC];
				}
			}
		}
	}
	free_coeff();	
	free(du);
	free(du_);
	free(dv);
	free(dv_);
	free(dw);
	free(dw_);
	free(RES_u);
	free(RHS_u);
	free(RES_v);
	free(RHS_v);
	free(RES_w);
	free(RHS_w);
}

void cleanup(){
	free(x);
	free(y);
	free(z);
	free(u);
	free(v);
	free(w);
	free(vort_x);
	free(vort_y);
	free(vort_z);
	free(vort_magn);
}

void output(){
	int i, j, k;
	FILE *out;
	out = fopen("init.dat","w");
  for (i = 0; i < NI; i++) {
		int p = c_m(i, 0, 0);
    fprintf(out,"%lf ",x[p]);
  }
  fprintf(out,"\n");
  for (j = 0; j < NJ; j++) {
		int p = c_m(0, j, 0);
    fprintf(out,"%lf ",y[p]);
  }
  fprintf(out,"\n");
  for (k = 0; k < NK; k++) {
		int p = c_m(0, 0, k);
    fprintf(out,"%lf ",z[p]);
  }
  fprintf(out,"\n");
	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int p = c_m(i, j, k);
				fprintf(out,"%lf ",u[p]);						
			}
		}
	}
  fprintf(out,"\n");
	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int p = c_m(i, j, k);
				fprintf(out,"%lf ",v[p]);						
			}
		}
	}
  fprintf(out,"\n");
	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int p = c_m(i, j, k);
				fprintf(out,"%lf ",w[p]);						
			}
		}
	}
  fprintf(out,"\n");
	fclose(out);
}

int main(){
	std::cout << "Generating Initial Velocity Field for Vortex-Ring problem...\n";
	inp();
	init();
	std::cout << "Initializing grid...\n";
	grid();
	std::cout << "Setting vorticity distribution...\n";
	set_vort();
	std::cout << "Solving for velocity... \n";
	solve_vel();
	std::cout << "Writing output file...\n";
	output();
	cleanup();
}
